## 1.3.0 - 16/11/2024

### Added

- #26: CLI option `--no-plot-display` to disable automatically showing generated figures when
  executing cavcalc via the command line. This allows saving generated plots, without forcibly
  showing them first.
- #25: Support for specifying units alongside argument as data-file. This also fixes a related
  issue (see below).
- A `--plot-opts` CLI option, and corresponding kwargs in the `Output.plot` method, to enable
  forwarding of plotting options (such as color, line-style, etc.) to matplotlib calls.
- Error handling for non-numeric arrays of data as arguments.
- NumPy v2 support.
- More type hints for API facing methods.

### Fixed

- #25: Correctly handling units for irregularly spaced array arguments via API.

## 1.2.0 - 08/05/2023

### Added

- #19: Support for cross-referencing parameters. One can now set a physical argument to point
  to another argument, resulting in that parameter taking on the same value(s) (and units).
- #23: Resonance enhancement factors (Airy relations), and fractional transmission intensity, as
  target parameters. These have the names: 'Aint', 'Aext', and 'Atrn' (see the [Quick Parameter Reference](https://cavcalc.readthedocs.io/en/stable/quick_param_ref.html)
  in the online documentation for details).
- #24: Allowing `Parameter` instances (i.e. both `ArgParameter` and `TargetParameter` objects) to be
  passed directly as arguments to the `calculate` API function.

### Changed

- #18: Improved how unused parameters get displayed in "Given:" blocks when calling `__str__` methods
  of Output objects. Also removed these unused parameters from legends / titles of figures when doing
  automated plotting.

### Fixed

- #21: Issuing error when a user specifies output units (via CLI option `-u`) for a target parameter
  which is dimensionless.
- #22: Now correctly handling irregularly spaced arrays passed as parameters to the `calculate` API function.

### Removed

- `Parameter.compact_str` property, as the new `Parameter.__str__` implementation replaces this.

## 1.1.0 - 12/04/2023

### Added

- Support for computing divergence angle for non-symmetric cavities (closes #10).
- A `BaseOutput` class, which `SingleOutput` and `MultiOutput` now derive from,
  for common functionality (closes #11).
- Documentation section on handling `Quantity` instances (closes #14).
- More validation (and other) tests, increasing coverage (closes #12).

### Changed

- Slightly modified round-trip Gouy phase versus g1, g2 (non-symmetric cavity)
  calculation; this will not affect results, just helps with performance and
  warning suppression for geometrically unstable cavities.
- Improved Quick Parameter Reference table in documentation, now using a pandas
  `DataFrame` with custom HTML / CSS for rendering this.

### Fixed

- Incorrect equation for divergence angle in symmetric cavities (closes #16).

## 1.0.0 - 13/01/2023

This version represents, essentially, a complete re-write of the core code. So, along with
the changes outlined below, there will be general reliability, bug fix, and performance improvements
in this release too.

### Added

- Improved units handling via [Pint](https://pint.readthedocs.io/en/stable/) support.
- Sorting of properties when printing `Output` objects.
- Serialisation of `Output` objects, both via the API and CLI.
- Mesh-grid option in CLI and API, allowing automatic computation of cavity
  properties over arbitrarily ordered grids.
- `cavcalc.configure` function for setting up temporary overrides of specific
  units, unit categories, and / or plotting defaults.
- The ability to specify beam sizes at the mirrors as arguments. This allows
  users to, e.g., compute the mirror radii of curvature from the beam sizes.
- Target functions for reflectivity and transmission of the cavity mirrors, as
  a function of each other and relative (power) loss.
- Improved error handling.
- Significantly improved plotting capabilities with `SingleOutput.plot` and
  `MultiOutput.plot` methods; these are also used automatically via the CLI (when
  `--plot` option, or similar, is specified).

### Changed

- Unified the CLI and API code, such that both now use `Output` objects and will
  act in identical ways when, e.g., printing and plotting results.
- Improved the `cavcalc.ini` config file. This now allows setting of units for
  different parameter categories, as well as individual parameter types.
- Removed the greater than two elements constraint on data range physical args.
- Fixed mode separation frequency calculations.
- Refactored most of the code.

### Removed

- The `--find` option from the CLI. This was a relatively obscure feature, but
  may be re-implemented in a future version.
- The `--dps` option from the CLI.
- Verbose "linspace" and "arange" forms of specifying data ranges in CLI. Now the
  single format is the previously recommended one: `"<start> <stop> <num>"`.
