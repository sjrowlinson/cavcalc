The files in this directory will be updated by the `poetry-export` stage
of the pre-commit hooks whenever a commit alters the `poetry.lock` file
in the root of the repository. In other words, these files should never
be modified manually.
