"""Custom exception types defined by ``cavcalc``."""

# TODO (sjr) May want more granular exception types deriving
#            from this in the future
class CavCalcError(Exception):
    """The base error class for all ``cavcalc`` exceptions."""

    pass
