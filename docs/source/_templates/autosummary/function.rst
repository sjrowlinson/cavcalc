:html_theme.sidebar_secondary.remove:

``{{ fullname }}``
{{ underline }}====

.. currentmodule:: {{ module }}

.. autofunction:: {{ name }}
