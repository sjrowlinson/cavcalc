.. cavcalc documentation master file

:html_theme.sidebar_secondary.remove:

.. only:: latex

    =======
    cavcalc
    =======

.. only:: not latex

    .. image:: _static/logo.svg
        :align: center
        :class: dark-light p-2

|

.. toctree::
    :maxdepth: 1
    :hidden:

    using/index
    api/index
    quick_param_ref
    details
    contributing/index


**Quick links**:
`Installation <using/installation.html>`_ |
`Source Repository <https://gitlab.com/sjrowlinson/cavcalc/>`_ |
`Issue Tracker <https://gitlab.com/sjrowlinson/cavcalc/-/issues>`_ |
`PyPI <https://pypi.org/project/cavcalc/>`_

Cavcalc is an open-source tool for computing parameters associated with Fabry-Perot optical
cavities. Both a CLI and Python API are provided for interacting with cavcalc. The CLI
represents a convenient way of quickly calculating / plotting cavity properties, whilst the API
can be used more broadly in a cavity / interferometer design and analysis workflow --- e.g. alongside
other programs such as `Finesse 3 <https://gitlab.com/ifosim/finesse/finesse3/>`_.

.. grid:: 3

   .. grid-item-card::
      :img-top: _static/getting_started.svg

      User Guide
      ^^^^^^^^^^

      The user guide is the best place to begin learning how to use cavcalc. It contains an installation
      guide, tutorials, and examples.

      +++

      .. button-ref:: using/index
         :expand:
         :color: secondary
         :click-parent:

         To the user guide

   .. grid-item-card::
      :img-top: _static/api.svg

      API Documentation
      ^^^^^^^^^^^^^^^^^

      The API reference contains detailed descriptions of the modules, objects, and functions
      included in cavcalc.

      +++

      .. button-ref:: api/index
         :expand:
         :color: secondary
         :click-parent:

         To the API documentation

   .. grid-item-card::
      :img-top: _static/contributor.svg

      Contributor's Guide
      ^^^^^^^^^^^^^^^^^^^

      Follow these general guides for reporting bugs, and more broadly contributing to cavcalc.

      +++

      .. button-link:: contributing/index.html
         :expand:
         :color: secondary
         :click-parent:

         To the contributor's guide
