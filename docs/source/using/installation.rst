.. _installation:

Installation
============

Release version
---------------

1. First create a suitable virtual environment, e.g.::

    python -m venv cavcalc_env

2. Activate this environment by running:

   .. tab-set::

        .. tab-item:: Unix / MacOS
            :sync: nixkey

            .. code:: bash

                source cavcalc_env/bin/activate

        .. tab-item:: Windows
            :sync: winkey

            .. code:: powershell

                cavcalc_env\Scripts\activate.bat

3. Once this virtual environment is activated, you can install ``cavcalc`` simply via::

    pip install cavcalc

4. You can check that the installation was successful with::

    cavcalc --version

   if you see something along the lines of::

    cavcalc v1.3.0

   then you should be ready to start using ``cavcalc``!

.. dropdown:: Troubleshooting for early adopters
    :icon: alert
    :color: warning
    :animate: fade-in-slide-down

    If you already have ``cavcalc`` installed on your machine, **with a version number less than v1.0.0** (i.e.
    an alpha version in this case), then you may run into issues with the change in the config file format
    from these earlier versions. To rectify this, you should delete the ``cavcalc.ini`` file present in your
    user config directory with the following command:

    .. tab-set::

        .. tab-item:: Unix / MacOS
            :sync: nixkey

            .. code:: bash

                rm ~/.config/cavcalc/cavcalc.ini

        .. tab-item:: Windows
            :sync: winkey

            Note the below assumes that you are using Windows PowerShell, but similar commands will exist
            for other terminals.

            .. code:: powershell

                Remove-Item "%APPDATA%\cavcalc\cavcalc.ini"

    Once that file is deleted, ``cavcalc`` will automatically generate the correct config file in the
    same location when running / importing again.


From source (development version)
---------------------------------

The steps below assume that you have a working `git` installation, and that the
environment you are using has Python version 3.9 as a minimum. The steps to
follow are:

1. Clone the repository. For example, via ssh::

    git clone git@gitlab.com:sjrowlinson/cavcalc.git

2. If you haven't done so already, install the Python package ``poetry``. The official
   instructions for doing so `can be found here <https://python-poetry.org/docs/#installation>`_.
   In short, on \*nix based systems the recommended command to run for installing ``poetry`` is::

    curl -sSL https://install.python-poetry.org | python3 -

3. (Optional) If you'd prefer that the virtual environment, which ``poetry`` makes when
   installing this package, is created within the project root, then run::

    poetry config virtualenvs.in-project true

   See `the documentation on this option <https://python-poetry.org/docs/configuration/#virtualenvsin-project>`_
   for more details.

4. Simply run::

    poetry install

   in the root of your local copy of the repository. This will create a virtual environment
   for ``cavcalc``; first installing its dependencies as defined in the ``poetry.lock`` file,
   then installing the package itself in editable mode. You can check that the installation
   was successful by activating the virtual environment (e.g. via ``poetry shell``), and running::

    cavcalc --version

   If this results in an output similar to::

    cavcalc v1.4.0.dev0

   then you should be ready to start using the development version of ``cavcalc``.
