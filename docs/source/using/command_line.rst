.. _command_line:

Using via the command line
==========================

Invoking ``cavcalc`` via the command line is a simple, and convenient, way to interact with
the program. For details on all the available options run::

    cavcalc -h

or just::

    cavcalc

Calculating single parameters
-----------------------------

By specifying a target to compute, ``cavcalc`` will show you the value of this property
given the input arguments you provided. For example, here we calculate the beam radius on
the mirrors of a symmetric cavity, given its length and radii of curvature of both mirrors::

    cavcalc w -L 1 -Rc 2.5

which results in:

.. jupyter-execute::
    :hide-code:

    import cavcalc as cc
    cc.configure("cavcalc")
    import numpy as np

    import matplotlib.pyplot as plt
    plt.rcParams["figure.figsize"] = [12, 7.416]
    plt.rcParams["font.size"] = 13

    print(cc.calculate("w", L=1, Rc=2.5))

Units can be given for both the target, and any input parameter::

    cavcalc modesep -u MHz -L 10cm -gouy 0.3rad

yielding:

.. jupyter-execute::
    :hide-code:

    with cc.configure(modesep="MHz"):
        print(cc.calculate("modesep", L="10cm", gouy="0.3rad"))

.. hint::

    Units support is provided via the package `Pint <https://pint.readthedocs.io/en/stable/index.html>`_, so
    any units defined in the Pint unit-registry can be used in cavcalc.

    So, yes, you can compute the FSR in units of 1/month for a cavity with a length in parsecs if you
    really want to::

        cavcalc FSR -u 1/month -L 0.67pc

    .. jupyter-execute::
        :hide-code:

        with cc.configure(fsr="1/month", L="pc"):
            print(cc.calculate("FSR", L=0.67))

    (but you probably shouldn't...)

    See the linked documentation above for an up-to-date reference on the relevant units defined in the registry.

Computing all available parameters
----------------------------------

The default behaviour of ``cavcalc``, i.e. when it is given no target, is to attempt to compute all the properties
that it possibly can from the set of input arguments provided. For example, we could request the properties of
a cavity based on knowledge of its length and mirror radii of curvature::

    cavcalc -L 33cm -Rc1 30cm -Rc2 20cm

.. jupyter-execute::
    :hide-code:

    print(cc.calculate(L="33cm", Rc1="30cm", Rc2="20cm"))

This is flexible in that, as usual, any valid argument can be used. Following on from the above example, we could
flip this around to instead compute all we can from the cavity length and beam radii on the mirrors::

    cavcalc -L 33cm -w1 542.85um -w2 212.92um

giving us values consistent with the above cavity:

.. jupyter-execute::
    :hide-code:

    print(cc.calculate(L="33cm", w1="542.85um", w2="212.92um"))

Note that, in this case, we also get a set of values corresponding to a different cavity geometry - i.e. on
the opposite side of the stability plot. This is simply a result of the equations which govern Fabry-Perot
cavity dynamics; and the varying cavity shapes that they represent (e.g. near-concentric vs. near plane-parallel
for the same beam radius values). The above output shows that ``cavcalc`` computes, and shows, both scenarios when
input values result in them.

.. dropdown:: Working with cavcalc.ini files --- local unit overrides
    :icon: workflow
    :color: info
    :animate: fade-in-slide-down

    The ``-u`` (units override for target) option only applies to single-target mode; i.e. when a target is
    specified. To override units for computed properties in multi-target mode, when invoking via the CLI, you
    should use a ``cavcalc.ini`` configuration file. This can be done by either:

    - modifying the config file located at ``$XDG_CONFIG_HOME/cavcalc/cavcalc.ini``, typically
      ``~/.config/cavcalc/cavcalc.ini``, on \*nix systems, or ``%APPDATA%\cavcalc\cavcalc.ini`` on Windows,
    - or making a new ``cavcalc.ini`` file in the current working directory (or wherever you are invoking
      ``cavcalc`` from). Note that this new file doesn't need to contain all the options that the file
      referenced above contains, you would only need to override the specific units that you want.

    You should prefer the latter option from above generally, as the former would result in behaviour changing
    for *any* usage of ``cavcalc`` on your system until such modifications are reversed. Using a new, trimmed
    down ``cavcalc.ini`` file in your working directory is the recommended approach for this use case.

    As an example, let's assume you are working in the directory ``~/work/local/cav_design_x``, and you want
    any ``cavcalc`` commands executed from this folder to use default units of mm for distances and mirror radii
    of curvatures, microns for beam radii, and GHz for frequencies. To do this, you could create a new file
    ``~/work/local/cav_design_x/cavcalc.ini`` with the following contents::

        [units]

        Beamsizes = um
        RoCs = mm
        Distances = mm
        Frequencies = GHz

    Then invoking ``cavcalc`` from this directory would use these unit category overrides as the defaults, e.g::

        cavcalc -L 56 -Rc1 25.5 -Rc2 35

    .. jupyter-execute::
        :hide-code:

        with cc.configure(beamsizes="um", rocs="mm", distances="mm", frequencies="GHz"):
            print(cc.calculate(L=56, Rc1=25.5, Rc2=35))

    Alternatively, you could use the Python API without needing to create or modify ``cavcalc.ini`` files. The
    function to use for this is :func:`.configure`, allowing for temporary overrides of parameter units within
    context-managed scopes. See :ref:`module` for more information on using ``cavcalc`` via the API.

Evaluating and plotting parameters over data-ranges
---------------------------------------------------

In addition to the scalar input values shown previously, ``cavcalc`` also supports a data-range
syntax for such input arguments; resulting in targets being computed over arrays of values. This
then enables automatic plotting of such targets via the CLI.

The data-range syntax is ``-param "start stop num [units]"``, where ``start``, ``stop`` are the
first and last values of the array, ``num`` is the number of points, and ``units`` (optional)
represents the physical units of the quantity. Values may also be read in from a file via
``-param <file>``; as long as the data in ``<file>`` represents a 1D array.

A few examples follow:

- Round-trip Gouy phase versus mirror radii of curvature::

    cavcalc gouy -L 12.4mm -Rc "10 20 499 mm" --plot

.. jupyter-execute::
    :hide-code:

    with cc.configure(L="mm", Rc="mm"):
        cc.calculate("gouy", L=12.4, Rc=np.linspace(10, 20, 499)).plot();

- Beam radius at the waist (on a log-scale) versus beam radius at the mirrors::

    cavcalc w0 -u um -L 15cm -w "200 1000 500 um" --logyplot

.. jupyter-execute::
    :hide-code:

    with cc.configure(L="cm", beamsizes="um"):
        cc.calculate("w0", L="15cm", w=np.linspace(200, 1000, 500)).plot(logy=True);

- Mode separation frequency versus both cavity length and g-factor of first mirror, with a fixed
  second mirror g-factor::

    cavcalc modesep -u MHz -L "20 30 200" -g1 "-1 0 200" -g2 -0.6 --plot --plot-opts "color: k, ls: --"

.. jupyter-execute::
    :hide-code:

    with cc.configure(modesep="MHz"):
        cc.calculate("modesep", L=np.linspace(20, 30, 200), g1=np.linspace(-1, 0, 200), g2=-0.6).plot(
            color="k", ls="--"
        );

Here we have used ``--plot-opts`` to specify plotting options to pass to the underlying matplotlib
plot call; in this case, setting the color of the trace to black, with a dashed line-style. One can
specify any valid option-value pairs using this CLI option, separating each with commas.

- FWHM versus mirror losses with fixed transmission values::

    cavcalc FWHM -L 4km -T1 0.014 -L1 "1e-5 1e-3 201" -T2 5e-6 -L2 "1e-5 1e-3 201" --logxplot

.. jupyter-execute::
    :hide-code:

    loss_arr = np.linspace(1e-5, 1e-3, )
    cc.calculate("FWHM", L="4km", T1=0.014, T2=5e-6, L1=loss_arr, L2=loss_arr).plot(logx=True);

.. hint::

    Arguments can be set to reference other parameters. In the last example above, the loss
    values at both mirrors are repeated - so we could instead write::

        cavcalc FWHM -L 4km -T1 0.014 -L1 "1e-5 1e-3 201" -T2 5e-6 -L2 L1 --logxplot

    to produce the same results.

    This referencing can be applied to any parameters with the same dimensionality, and can
    be used to reference data-ranges (as we saw above) or scalar values. One can also
    reference parameters which themselves are references, so long as there are no self-references
    or loops in the referencing chain.

Grid evaluation and image plots
-------------------------------

Parameters with multiple dependencies can be computed over more than just a single dimension. By using
the ``--mesh`` option, one can specify grids over which to compute cavity properties. If multiple
data-range arguments are given without meshing, then the target(s) will be computed over these
simultaneously; as we saw in the last two examples of the previous section.

The ``--mesh`` argument can be set to ``true``, such that any data-range arguments are meshed together
in the order that they were given, or it can be given as ``--mesh "param1,param2,..."`` where the ``param<n>``
values here represent the names of the data-range parameters specified. Multiple mesh-grids over different
parameter combination can be defined by delimiting with ``;``, e.g: ``--mesh "g1,g2;L,R1,R2"`` would create
a mesh-grid of parameters ``g1`` and ``g2``, as well as another separate mesh of parameters ``L``, ``R1``,
and ``R2``, respectively.

Here are a few examples of this in action:

- Beam radius at mirrors versus the cavity length and round-trip Gouy phase on a grid::

    cavcalc w -L "1 10 100 km" -gouy "20 120 100 deg" --mesh true --plot

.. jupyter-execute::
    :hide-code:

    with cc.configure(L="km"):
        cc.calculate("w", L=np.linspace(1, 10, 100), gouy=np.linspace(20, 120, 100), meshes=True).plot();

- Waist radius versus the individual mirror g-factors on a grid, with a colour map specified, and using a log-scale::

    cavcalc w0 -L 1 -g1 "-2 2 399" -g2 g1 --mesh true --logplot --cmap nipy_spectral

.. jupyter-execute::
    :hide-code:

    cc.calculate("w0", L=1, g1=np.linspace(-2, 2, 399), g2="g1", meshes=True).plot(logz=True, cmap="nipy_spectral");

- Divergence angle versus the beam radius at the mirrors, and the wavelength of the beam, on a grid. Notice here that ``wl`` was
  specified before ``w``, but we used ``--mesh "w,wl"`` to explicitly declare that the mesh-grid should constructed in this
  order; resulting in ``w`` being the x-axis parameter on the plots below::

    cavcalc div -L 34mm -wl "1 2 200 um" -w "150 300 200 um" --mesh "w,wl" --plot -u arcmin

.. jupyter-execute::
    :hide-code:

    with cc.configure(L="mm", wl="um", w="um", div="arcmin"):
        fig = cc.calculate("div", L=34, wl=np.linspace(1, 2, 200), w=np.linspace(150, 300, 200), meshes=("w", "wl")).plot(show=False);
        fig.subplots_adjust(wspace=0.25)

- Pole frequency versus mirror losses on a grid, with fixed transmission values::

    cavcalc pole -u GHz -L 67um -L1 "1e-6 1e-4 200" -T1 0.02 -L2 L1 -T2 1e-4 --plot --mesh true --cmap cividis

.. jupyter-execute::
    :hide-code:

    loss_arr = np.linspace(1e-6, 1e-4, 200)
    with cc.configure(pole="GHz"):
        cc.calculate("pole", L="67um", T1=0.02, T2=1e-4, L1=loss_arr, L2=loss_arr, meshes=True).plot(cmap="cividis");

.. note::

    You can also use data-ranges, and meshes, when no target is specified (i.e. for computing all available
    parameters over the arrays given). This works identically to the single target mode, with no change in
    syntax for invoking ``cavcalc``. By using the ``--plot`` option (or the log-plot alternatives) in this
    mode, you will get separate figures for each target which was computed as an array.

    This will often result in quite a few figures being produced when some common arguments (like cavity length
    and mirror radii of curvature) are given, so in the interest of saving space on this page, a short example
    is shown below; where we only specify an array of values for ``gs`` and give nothing else to ``cavcalc``::

        cavcalc -gs "-1 1 201" --plot

    .. jupyter-execute::
        :hide-code:

        cc.calculate(gs=np.linspace(-1, 1, 201)).plot();

Saving and loading ``cavcalc`` results
--------------------------------------

The CLI provides two arguments, ``"-save"`` and ``"-load"``, which can be used for saving
the output to a binary file, and loading an output from such a file, respectively. Any
extension may be given to the file when saving it. The standard Python package `pickle <https://docs.python.org/3/library/pickle.html>`_
is used for supporting serialisation of the output objects produced by ``cavcalc``.

The default behaviour when loading is to just get the output object "as-is" from the file; so
you do not need to re-specify the target if the loaded file represents an output object from
a previous single-target mode call. If you do specify a target, but this does not match the
one computed in the output file, then an error message will be shown.

You can specify a single target when loading a file representing a multi-target output
object (so long as that target was computed), e.g::

    cavcalc -L 10cm -Rc1 6cm -Rc2 7.85cm -save "10cm_resonator.cav"

The above saves a multi-target output object to a file ``"10cm_resonator.cav"`` within the
current working directory. Then we can show just the beam radius at the waist from this
file via::

    cavcalc w0 -u um -load "10cm_resonator.cav"

.. jupyter-execute::
    :hide-code:

    with cc.configure(w0="um", L="cm", rocs="cm"):
        print(cc.calculate("w0", L=10, Rc1=6, Rc2=7.85))
