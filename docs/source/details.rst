.. _details:

Equation Reference
==================

All of the underlying equations used by cavcalc are given in :mod:`cavcalc.functions`. You may
note that there are only relatively few functions listed, for example: there is no direct
function (and so no documented equation) for computing the beam radius from the round-trip Gouy
phase. This is because a primary feature of cavcalc is to perform "target chaining" whereby new
targets are computed from those targets which were calculated via the initial set of arguments
given; think of this just like deriving the equations dynamically.

A future version of cavcalc may support symbolic expressions, such that one can obtain
objects representing parameters which can be expressed in terms of any their
dependencies (either directly or via target chaining).
