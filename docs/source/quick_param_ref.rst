.. _param_ref:

Quick Parameter Reference
=========================

The table below gives an overview of the Fabry-Perot cavity parameters used
by ``cavcalc``, with details on their names (e.g. for providing them as ``**kwargs``
to :func:`.calculate` or as CLI options), corresponding :class:`.ParameterType` and
:class:`.ParameterCategory` literals, and whether they can be used as arguments and / or targets.

.. jupyter-execute::
    :hide-code:

    import cavcalc.parameters as ccp
    import pandas as pd

    args = ccp.valid_arguments
    tgts = ccp.valid_targets

    inline_html = lambda s: f"<code class=\"docutils literal notranslate\"><span class=\"pre\">{s}</span></code>"
    t_or_c = lambda b: "\u2713" if b else "\u2717"

    pd.DataFrame(
        [
            (p.name, inline_html(p.ptype.name), inline_html(p.category.name), t_or_c(p.name in args), t_or_c(p.name in tgts))
            for p in sorted(
                (ccp.ArgParameter(name) for name in ccp.tools.get_names()),
                key=lambda p: (p.category.value, p.ptype.value)
            )
        ],
        columns=["Name", "ParameterType", "ParameterCategory", "Argument?", "Target?"]
    ).style\
        .hide()\
        .applymap(lambda v, props='': props if v == "\u2713" else None, props="color:darkgreen")\
        .applymap(lambda v, props='': props if v == "\u2717" else None, props="color:darkred")\
        .set_table_styles([
            {"selector": "th.col_heading", "props": 'text-align: center'},
            {"selector": "td", "props": "padding: 10px; text-align: center"},
            {"selector": "th", "props": "padding-left: 40px; padding-right: 40px; background-color: grey; text-align: center"},
            {"selector": "tr", "props": "border: solid; border-width: 0.5px 0"},
        ])
