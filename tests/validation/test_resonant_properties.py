import cavcalc as cc
from cavcalc.errors import CavCalcError
from cavcalc.output import SingleOutput
import pytest

from ._utils import make_quantity


_C_LIGHT = 299792458.0


@pytest.mark.parametrize("L", (1, "5mm", "10km", "1pm", "3in"))
def test_fsr(L):
    out = cc.calculate("FSR", L=L)

    assert isinstance(out, SingleOutput)
    assert out.result.value.units == cc.ureg.Unit("Hz")

    L = make_quantity("L", L)
    assert out.result.value.to_base_units().m == pytest.approx(_C_LIGHT / (2 * L.to_base_units().m))


@pytest.mark.parametrize("compute", ("FSR", "FWHM", "pole"))
@pytest.mark.parametrize("units", ("m", "mm", "Gm", "ft"))
def test_freq_raises_with_length_zero(compute, units):
    with pytest.raises(CavCalcError, match=f"Division by zero.*{compute.lower()}"):
        cc.calculate(compute, L=f"0{units}", R1=0.9, R2=0.9)
