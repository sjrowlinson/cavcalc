import cavcalc as cc
from cavcalc.errors import CavCalcError
from cavcalc.output import SingleOutput
import numpy as np
import pytest

from ._utils import make_quantity


@pytest.mark.parametrize("L", (1, "4km", "3.14mm", 0))
@pytest.mark.parametrize("wl", (1064, "1550nm", "2.09um"))
def test_beamsize_with_length_gs_zero(L, wl):
    out = cc.calculate("w", L=L, gs=0, wl=wl)

    L = make_quantity("L", L)
    wl = make_quantity("wl", wl)

    assert isinstance(out, SingleOutput)
    expect = np.sqrt(L.to_base_units().m * wl.to_base_units().m / np.pi)  # w(L, wl, gs=0)

    assert out.result.value.to_base_units().m == pytest.approx(expect)


@pytest.mark.parametrize("L", (10, "1um", "40km"))
@pytest.mark.parametrize("wl", (1550, "0.5um"))
@pytest.mark.parametrize("gs", (0.5, 0.99, -0.01, -0.8))
def test_beamsize_with_length_symmetric_gs(L, wl, gs):
    out_gs_neg = cc.calculate("w", L=L, wl=wl, gs=-gs)
    out_gs_pos = cc.calculate("w", L=L, wl=wl, gs=gs)

    assert isinstance(out_gs_neg, SingleOutput)
    assert isinstance(out_gs_pos, SingleOutput)

    assert out_gs_pos.result.value.m == pytest.approx(out_gs_neg.result.value.m, abs=1e-14)


@pytest.mark.parametrize("L", ("30mm", 0))
@pytest.mark.parametrize("gs", (-1, 1))
def test_beamsize_with_gs_mag_unity_raises(L, gs):
    with pytest.raises(CavCalcError, match=r"Division by zero.*target: 'w'"):
        cc.calculate("w", L=L, gs=gs)


@pytest.mark.parametrize("L", ("0.1cm", 0))
@pytest.mark.parametrize("gs", (-1.1, 12.2))
def test_beamsize_nan_with_gs_mag_gtr_unity(L, gs):
    out = cc.calculate("w", L=L, gs=gs)

    assert isinstance(out, SingleOutput)

    assert np.isnan(out.result.value.m)


@pytest.mark.parametrize("L", ("2in", "99um", "12mm"))
@pytest.mark.parametrize("Rc", (12.12, "5cm", "1.1in"))
def test_beamsize_chained_from_roc(L, Rc):
    out = cc.calculate("w", L=L, Rc=Rc)

    L = make_quantity("L", L)
    Rc = make_quantity("Rc", Rc)
    gs = 1 - L / Rc

    out_expect = cc.calculate("w", L=L, gs=gs)

    assert out.result.value.m == pytest.approx(out_expect.result.value.m)


@pytest.mark.parametrize("L", (1, "4km", "3.14mm", 0))
@pytest.mark.parametrize("wl", (1064, "1550nm", "2.09um"))
@pytest.mark.parametrize("gouy", (180, (np.pi, "rad")))
def test_beamsize_with_length_gouy_pi_rad(L, wl, gouy):
    out = cc.calculate("w", L=L, wl=wl, gouy=gouy)

    L = make_quantity("L", L)
    wl = make_quantity("wl", wl)

    assert isinstance(out, SingleOutput)
    expect = np.sqrt(L.to_base_units().m * wl.to_base_units().m / np.pi)  # w(L, wl, gouy=pi)

    assert out.result.value.to_base_units().m == pytest.approx(expect)


@pytest.mark.parametrize("L", ("3km", "19mm"))
@pytest.mark.parametrize("wl", (3.3e3, "0.44um"))
def test_waistsize_with_length_gs_zero(L, wl):
    out = cc.calculate("w0", L=L, wl=wl, gs=0)

    assert isinstance(out, SingleOutput)

    L = make_quantity("L", L)
    wl = make_quantity("wl", wl)
    expect = np.sqrt(0.5 * L.to_base_units().m * wl.to_base_units().m / np.pi)  # w0(L, wl, gs=0)

    assert out.result.value.to_base_units().m == pytest.approx(expect)


@pytest.mark.parametrize("L", ("3ft", 1e-4))
@pytest.mark.parametrize("wl", (1e3, "4um"))
def test_waistsize_zero_with_length_gs_neg_unity(L, wl):
    out = cc.calculate("w0", L=L, wl=wl, gs=-1)

    assert isinstance(out, SingleOutput)

    assert out.result.value.m == 0


@pytest.mark.parametrize("L", ("30nm", 0))
def test_waistsize_with_gs_unity_raises(L):
    with pytest.raises(CavCalcError, match=r"Division by zero.*target: 'w0'"):
        cc.calculate("w0", L=L, gs=1)


@pytest.mark.parametrize("L", ("2in", "99um", "12mm"))
@pytest.mark.parametrize("Rc", (12.12, "5cm", "1.1in"))
def test_waistsize_chained_from_roc(L, Rc):
    out = cc.calculate("w0", L=L, Rc=Rc)

    L = make_quantity("L", L)
    Rc = make_quantity("Rc", Rc)
    gs = 1 - L / Rc

    out_expect = cc.calculate("w0", L=L, gs=gs)

    assert out.result.value.m == pytest.approx(out_expect.result.value.m)


@pytest.mark.parametrize("gs", (0.5, 0.3, -0.99, -1e-5))
def test_gouy_phase_quadrants(gs):
    """Check that round-trip Gouy phase is in correct quadrant based
    on signs of g-factors."""
    out = cc.calculate("gouy", gs=gs)
    gouy = out.result.value.m

    if gs > 0:
        assert gouy < 180
    else:
        assert gouy > 180


@pytest.mark.parametrize("gs", (0, 1, -1))
def test_critical_gouy_phases(gs):
    out = cc.calculate("gouy", gs=gs)
    gouy = out.result.value.m

    if gs == 0:
        assert gouy == 180
    elif gs == 1:
        assert gouy == 0
    else:
        assert gouy == 360
