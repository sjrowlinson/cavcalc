import cavcalc as cc


def make_quantity(p, value):
    """Construct a Quantity of the given parameter `p`, with
    a magnitude of `value`. If `value` is not a str, then
    the Quantity is construct with the default units of `p`."""
    if isinstance(value, str):
        return cc.Q_(value)

    return cc.Q_(value, cc.get_default_units(p))
