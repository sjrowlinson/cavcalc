import cavcalc as cc
from cavcalc.errors import CavCalcError
import numpy as np
import pytest

from ._utils import make_quantity


@pytest.mark.parametrize("L", (1, "4km", "30um", "12in"))
@pytest.mark.parametrize("wl", (1064, "1550nm", "2.09um"))
@pytest.mark.parametrize(
    "g1g2", ((0.5, 0.5), (-0.9, -0.1), (0.999, 0.998), (-0.0001, -0.8004), (0.75, 0.24))
)
def test_gfactor_beamsize_ratio(L, wl, g1g2):
    """Check that output beam-sizes obey g1 / g2 = (w2 / w1)**2 relation."""
    g1, g2 = g1g2
    out = cc.calculate(L=L, wl=wl, g1=g1, g2=g2)

    w1 = out["w1"].value
    w2 = out["w2"].value

    assert g1 / g2 == pytest.approx((w2.m / w1.m) ** 2)


@pytest.mark.parametrize("L", (1,))
@pytest.mark.parametrize("wl", (1064, "1550nm", "2.09um"))
@pytest.mark.parametrize("w1w2", (("1mm", "1mm"), ("750um", "1.2mm"), ("2.5mm", "673.3um")))
def test_beamsize_gfactor_ratio(L, wl, w1w2):
    """Check that output g-factors obey g1 / g2 = (w2 / w1)**2 relation."""
    w1, w2 = w1w2
    w1 = make_quantity("w1", w1)
    w2 = make_quantity("w2", w2)

    out = cc.calculate(L=L, wl=wl, w1=w1, w2=w2)

    g1 = out["g1"].value
    g2 = out["g2"].value

    assert g1.m / g2.m == pytest.approx((w2.to_base_units().m / w1.to_base_units().m) ** 2)


@pytest.mark.parametrize("g1", (0.5, 0.3, -0.99, -1e-5))
@pytest.mark.parametrize("g2", (-0.5, -0.789, 1e-7, 0.9994))
def test_gouy_phase_quadrants(g1, g2):
    """Check that round-trip Gouy phase is in correct quadrant based
    on signs of g-factors."""
    out = cc.calculate("gouy", g1=g1, g2=g2)
    gouy = out.result.value.m

    if g1 > 0 and g2 > 0:
        assert gouy < 180
    elif g1 < 0 and g2 < 0:
        assert gouy > 180
    else:
        assert np.isnan(gouy)


@pytest.mark.parametrize("L", (1, "4km", "30um", "12in"))
@pytest.mark.parametrize("g1g2", ((0.5, 0.5), (-0.9, -0.9), (0.999, 0.999), (-0.0001, -0.0001)))
def test_waistpos_with_equal_gfactors(L, g1g2):
    """Check that the beam waist position is half the cavity length if
    the g-factors are equal."""
    g1, g2 = g1g2
    out = cc.calculate("z0", L=L, g1=g1, g2=g2)

    z0 = out.result.value.to_base_units().m
    L = out.given["L"].value.to_base_units().m

    assert z0 == pytest.approx(L / 2)


@pytest.mark.parametrize("w", ("w1", "w2"))
@pytest.mark.parametrize("L", ("3.33cm", "0km"))
@pytest.mark.parametrize("g1g2", ((1, 1), (-1, -1)))
def test_beamsize_gfactors_mag_unity_raises(w, L, g1g2):
    """Make sure a division by zero error is raised, when computing
    the beam size at either mirror, if the g-factors are both unity
    or negative unity."""
    g1, g2 = g1g2
    with pytest.raises(CavCalcError, match=f"Division by zero.*target: '{w}'"):
        cc.calculate(w, L=L, g1=g1, g2=g2)
