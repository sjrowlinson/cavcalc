"""Check that passing arguments as arrays works as expected."""

import cavcalc as cc
import numpy as np
import pytest


@pytest.mark.parametrize(
    "args",
    (
        {"L": np.linspace(1, 10, 11), "gs": 0.5},
        {"L": "3cm", "R1": np.linspace(0.5, 0.99, 35), "R2": 0.99},
        {"w": np.arange(100, 200, 2.5), "L": "8.5cm"},
        {"L": "10km", "g1": -1.1, "g2": -0.89, "wl": np.linspace(550, 2090, 3)},
    ),
)
def test_array_arg_regular_spacing(args):
    arr_args = {k: v for k, v in args.items() if isinstance(v, np.ndarray)}

    out = cc.calculate(**args)

    for pname, values in arr_args.items():
        assert out.given[pname].value.u == cc.ureg.Unit(cc.get_default_units(pname) or "")
        assert np.all(out.given[pname].value.m == values)


@pytest.mark.parametrize(
    "arg",
    (
        {"L": (np.linspace(1, 3, 33), "in")},
        {"Rc": (np.linspace(0.1, 0.9, 21), "km")},
        {"gouy": (np.linspace(0, 2 * np.pi, 40), "rad")},
    ),
)
def test_array_arg_regular_spacing_with_units(arg):
    pname = tuple(arg.keys())[0]
    values, units = tuple(arg.values())[0]
    out = cc.calculate(**arg)

    assert out.given[pname].value.u == cc.ureg.Unit(units)
    assert np.all(out.given[pname].value.m == values)


@pytest.mark.parametrize(
    "args",
    (
        {"L": np.logspace(1, 3, 11), "gs": 0.5},
        {"L": "3cm", "R1": np.logspace(-1, 0, 35), "R2": 0.99},
        {"w": np.array([100, 120, 150, 190, 195, 199, 200]), "L": "8.5cm"},
        {"L": "10km", "g1": -1.1, "g2": -0.89, "wl": np.array([550, 1064, 2090])},
    ),
)
def test_array_arg_irregular_spacing(args):
    """Irregular arrays follow a different code-path as the cc.calculate call
    needs to create tmp files for these (due to data-range syntax inherently
    not supporting irregularly spaced arrays)."""
    arr_args = {k: v for k, v in args.items() if isinstance(v, np.ndarray)}

    out = cc.calculate(**args)

    for pname, values in arr_args.items():
        assert out.given[pname].value.u == cc.ureg.Unit(cc.get_default_units(pname) or "")
        assert np.all(out.given[pname].value.m == values)


@pytest.mark.parametrize(
    "arg",
    (
        {"L": (np.logspace(1, 3, 33), "in")},
        {"Rc": (np.logspace(-2, 0, 21), "km")},
        {"gouy": (np.array([0, np.pi / 2, 7 * np.pi / 4, 11 * np.pi / 4]), "rad")},
    ),
)
def test_array_arg_irregular_spacing_with_units(arg):
    pname = tuple(arg.keys())[0]
    values, units = tuple(arg.values())[0]
    out = cc.calculate(**arg)

    assert out.given[pname].value.u == cc.ureg.Unit(units)
    assert np.all(out.given[pname].value.m == values)
