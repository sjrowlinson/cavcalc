"""Test plot creation via the API."""

import builtins
import cavcalc as cc
from cavcalc.errors import CavCalcError
from cavcalc.output import SingleOutput, MultiOutput
import io
from matplotlib.testing.decorators import check_figures_equal
import numpy as np
import os
import pytest


def patch_open(open_func, files):
    def open_patched(
        path,
        mode="r",
        buffering=-1,
        encoding=None,
        errors=None,
        newline=None,
        closefd=True,
        opener=None,
    ):
        if "w" in mode and not os.path.isfile(path):
            files.append(path)

        return open_func(
            path,
            mode=mode,
            buffering=buffering,
            encoding=encoding,
            errors=errors,
            newline=newline,
            closefd=closefd,
            opener=opener,
        )

    return open_patched


@pytest.fixture(autouse=True)
def cleanup_files(monkeypatch):
    files = []
    monkeypatch.setattr(builtins, "open", patch_open(builtins.open, files))
    monkeypatch.setattr(io, "open", patch_open(io.open, files))

    yield

    for file in files:
        if os.path.isfile(file) and str(file).endswith(".png"):
            os.remove(file)


def _make_ax_label(p):
    return p.description + (f" [{p.value.units:~}]" if not p.is_unitless else "")


def _make_legend_label(out: SingleOutput, xp, yp, zp=None):
    if fixed := tuple(filter(lambda p: p.is_scalar and p.was_used, out.given.values())):
        fixed_params_str = "with: " + ", ".join(f"{p.symbol_str} = {p.value:~}" for p in fixed)
    else:
        fixed_params_str = ""

    if zp:
        return f"{zp.symbol_str}({xp.symbol_str}, {yp.symbol_str}) " + fixed_params_str

    if isinstance(xp, tuple):
        xp1, xp2 = xp
        return f"{yp.symbol_str}({{{xp1.symbol_str}, {xp2.symbol_str}}}) " + fixed_params_str

    return f"{yp.symbol_str}({xp.symbol_str}) " + fixed_params_str


@check_figures_equal(extensions=("png",))
@pytest.mark.parametrize("target", ("w", "w0", "div"))
@pytest.mark.parametrize(
    "args",
    (
        {"L": 1, "gs": np.linspace(-1, 1, 99)},
        {"L": (np.linspace(3, 30, 33), "um"), "gs": -0.8, "logx": True},
        {"L": "4km", "Rc": np.linspace(2250, 2500, 50), "logy": True},
        {"L": 0.1, "gouy": np.linspace(280, 320, 49), "logx": True, "logy": True},
    ),
)
@pytest.mark.parametrize("plt_style", ("cavcalc", None))
def test_line_plots_single_target(fig_test, fig_ref, target, args, plt_style):
    logx = args.pop("logx", False)
    logy = args.pop("logy", False)
    with cc.configure(plt_style=plt_style):
        out = cc.calculate(target, **args)

        assert isinstance(out, SingleOutput)

        out.plot(show=False, fig=fig_test, logx=logx, logy=logy)

        x_arg = next((x for x in out.given.values() if x.is_array))
        y_arg = out.result

        if logx and logy:
            plot_func = fig_ref.subplots().loglog
        elif logx:
            plot_func = fig_ref.subplots().semilogx
        elif logy:
            plot_func = fig_ref.subplots().semilogy
        else:
            plot_func = fig_ref.subplots().plot

        plot_func(x_arg.value.m, y_arg.value.m, label=_make_legend_label(out, x_arg, y_arg))
        fig_ref.axes[0].legend()
        fig_ref.axes[0].set_xlabel(_make_ax_label(x_arg))
        fig_ref.axes[0].set_ylabel(_make_ax_label(y_arg))


@check_figures_equal(extensions=("png",))
@pytest.mark.parametrize(
    "args",
    (
        ("w0", {"L": 10, "wl": (np.linspace(1, 2, 100), "um"), "gs": np.linspace(-0.9, -0.5, 100)}),
        (
            "w0",
            {
                "L": 10,
                "gs": np.linspace(-0.9, -0.5, 100),
                "wl": (np.linspace(1, 2, 100), "um"),
                "logy": True,
            },
        ),
        (
            "FWHM",
            {
                "L": "4km",
                "T1": 0.014,
                "T2": 5e-6,
                "L1": np.linspace(1e-5, 1e-3, 201),
                "L2": "L1",
                "logx": True,
            },
        ),
        (
            "modesep",
            {
                "L": np.linspace(20, 30, 200),
                "g1": np.linspace(0, 1, 200),
                "g2": 0.6,
                "logx": True,
                "logy": True,
            },
        ),
    ),
)
def test_dual_axis_line_plots_single_target(fig_test, fig_ref, args):
    import matplotlib.pyplot as plt

    target, kwargs = args
    logx = kwargs.pop("logx", False)
    logy = kwargs.pop("logy", False)
    out = cc.calculate(target, **kwargs)

    out.plot(show=False, fig=fig_test, logx=logx, logy=logy)

    given = out.given.values()
    x1, x2 = tuple(sorted(filter(lambda p: p.is_array, given), key=lambda p: (p.axis, p.index)))
    y_arg = out.result

    if logx and logy:
        plot_func = fig_ref.subplots().loglog
    elif logx:
        plot_func = fig_ref.subplots().semilogx
    elif logy:
        plot_func = fig_ref.subplots().semilogy
    else:
        plot_func = fig_ref.subplots().plot

    plot_func(x1.value.m, y_arg.value.m, label=_make_legend_label(out, (x1, x2), y_arg))
    fig_ref.axes[0].legend()
    fig_ref.axes[0].set_xlabel(_make_ax_label(x1))
    fig_ref.axes[0].set_ylabel(_make_ax_label(y_arg))

    ax2 = plt.twiny(ax=fig_ref.axes[0])

    if logx and logy:
        plot_func2 = ax2.loglog
    elif logx:
        plot_func2 = ax2.semilogx
    elif logy:
        plot_func2 = ax2.semilogy
    else:
        plot_func2 = ax2.plot

    plot_func2(x2.value.m, y_arg.value.m, alpha=0)
    ax2.set_xlabel(_make_ax_label(x2))


@pytest.mark.parametrize(
    "args",
    (
        (
            "w0",
            {"L": 10, "wl": (np.linspace(1, 2, 100), "um"), "gs": 0.5},
            {"xlim": "data", "ylim": "data"},
        ),
        (
            "div",
            {"L": 0.1, "gouy": np.linspace(280, 320, 49)},
            {"xlim": (300, 310), "ylim": (0.28, 0.32)},
        ),
    ),
)
def test_axis_limits_overrides_single_target(args):
    import matplotlib.pyplot as plt

    target, kwargs, lims = args
    out = cc.calculate(target, **kwargs)

    fig = out.plot(show=False, xlim=lims["xlim"], ylim=lims["ylim"])

    x_arg = next((x for x in out.given.values() if x.is_array))
    y_arg = out.result

    expect_xlim = lims["xlim"]
    if expect_xlim == "data":
        expect_xlim = x_arg.value.m.min(), x_arg.value.m.max()

    expect_ylim = lims["ylim"]
    if expect_ylim == "data":
        expect_ylim = y_arg.value.m.min(), y_arg.value.m.max()

    assert fig.axes[0].get_xlim() == expect_xlim
    assert fig.axes[0].get_ylim() == expect_ylim

    plt.close(fig)


@pytest.mark.parametrize(
    "args",
    (
        {"L": 1, "gs": np.linspace(-1, 1, 99)},
        {"L": "10km", "R1": np.linspace(0.8, 1, 25), "R2": 0.99},
        {"L": "33.3cm", "w1": np.linspace(1, 100, 80), "w2": 54},
    ),
)
def test_line_plots_exist_all_targets(args, tmp_path):
    import matplotlib.pyplot as plt

    out = cc.calculate(**args)

    assert isinstance(out, MultiOutput)

    filename = "out"
    figs = out.plot(show=False, filename=str(tmp_path / filename))

    array_results = tuple(tp for tp in filter(lambda tp: tp.is_array, out.results.values()))

    assert set(tp.ptype for tp in array_results) == set(figs.keys())

    for tp in array_results:
        assert os.path.isfile(tmp_path / f"{filename}_{tp.name}.png")

    for fig in figs.values():
        plt.close(fig)


@pytest.mark.parametrize("target", ("w", "w0", "div"))
@pytest.mark.parametrize(
    "args",
    (
        {"L": 1, "gs": -0.4},
        {"L": "4km", "Rc": "2.3km", "R1": np.linspace(0.1, 0.9, 11)},
        {"L": 0.1, "gouy": 19, "L2": np.logspace(-1, 0, 20)},
    ),
)
def test_plot_raises_with_no_array_args(target, args):
    out = cc.calculate(target, **args)

    with pytest.raises(CavCalcError, match="Value of target.*is not array-like"):
        out.plot(show=False)


@pytest.mark.parametrize(
    "args",
    (
        ("FWHM", {"L": np.linspace(25, 50), "R1": np.linspace(0.8, 0.9), "R2": "R1"}),
        (
            "w0",
            {
                "L": "56cm",
                "wl": np.linspace(1e3, 2e3, 20),
                "g1": np.linspace(-1.2, -0.6, 20),
                "g2": "g1",
            },
        ),
    ),
)
def test_plot_raises_with_gtr2_x_args(args):
    target, kwargs = args
    out = cc.calculate(target, **kwargs)

    with pytest.raises(CavCalcError, match="Plots with more than two x-axes are not supported"):
        out.plot(show=False)


@check_figures_equal(extensions=("png",))
@pytest.mark.parametrize("target", ("w0", "div"))
@pytest.mark.parametrize(
    "args",
    (
        {"L": np.linspace(1, 10, 99), "gs": np.linspace(-1, 1, 99)},
        {
            "L": (np.linspace(3, 33), "cm"),
            "gouy": np.linspace(20, 160),
            "logz": True,
        },
        {
            "L": "4.5cm",
            "g1": np.linspace(-0.8, 0.8, 49),
            "g2": np.linspace(-0.8, 0.8, 49),
        },
        # TODO (sjr) Add test case with flipped mesh order
    ),
)
@pytest.mark.parametrize("plt_style", ("cavcalc", None))
def test_image_plots_single_target(fig_test, fig_ref, target, args, plt_style):
    import matplotlib.pyplot as plt

    logz = args.pop("logz", False)
    with cc.configure(plt_style=plt_style):
        args.setdefault("meshes", True)
        out = cc.calculate(target, **args)

        assert isinstance(out, SingleOutput)

        out.plot(show=False, fig=fig_test, logz=logz)

        arr_args = tuple(filter(lambda p: p.is_array, out.given.values()))
        x_arg, y_arg = arr_args
        z_arg = out.result

        extent = [
            x_arg.value.m.min(),
            x_arg.value.m.max(),
            y_arg.value.m.min(),
            y_arg.value.m.max(),
        ]

        norm = plt.cm.colors.LogNorm() if logz else None
        surf = fig_ref.subplots().imshow(
            z_arg.value.m.T,
            extent=extent,
            origin="lower",
            aspect="auto",
            norm=norm,
        )
        fig_ref.axes[0].set_xlabel(_make_ax_label(x_arg))
        fig_ref.axes[0].set_ylabel(_make_ax_label(y_arg))
        fig_ref.axes[0].set_title(_make_legend_label(out, x_arg, y_arg, z_arg))
        plt.colorbar(
            surf, ax=fig_ref.axes[0], label=_make_ax_label(z_arg), fraction=0.046, pad=0.04
        )


@check_figures_equal(extensions=("png",))
@pytest.mark.parametrize(
    "args", (("w0", {"L": "3cm", "w1": (np.linspace(80, 150, 99), "um"), "w2": "w1"}),)
)
@pytest.mark.parametrize("plt_style", ("cavcalc", None))
def test_dual_image_plots_single_target(fig_test, fig_ref, args, plt_style):
    import matplotlib.pyplot as plt

    target, kwargs = args
    logz = kwargs.pop("logz", False)
    with cc.configure(plt_style=plt_style):
        kwargs.setdefault("meshes", True)
        out = cc.calculate(target, **kwargs)

        assert isinstance(out, SingleOutput)

        out.plot(show=False, fig=fig_test, logz=logz)

        arr_args = tuple(filter(lambda p: p.is_array, out.given.values()))
        x_arg, y_arg = arr_args
        z_arg = out.result

        extent = [
            x_arg.value.m.min(),
            x_arg.value.m.max(),
            y_arg.value.m.min(),
            y_arg.value.m.max(),
        ]

        norm = plt.cm.colors.LogNorm() if logz else None

        ax1 = fig_ref.add_subplot(121)
        ax2 = fig_ref.add_subplot(122)

        z1, z2 = z_arg.value.m

        title = _make_legend_label(out, x_arg, y_arg, z_arg)
        surf1 = ax1.imshow(z1.T, extent=extent, norm=norm, origin="lower", aspect="auto")
        plt.colorbar(surf1, ax=ax1, label=_make_ax_label(z_arg), fraction=0.09, pad=0.04)

        ax1.set_xlabel(_make_ax_label(x_arg))
        ax1.set_ylabel(_make_ax_label(y_arg))
        ax1.set_title(title)

        surf2 = ax2.imshow(z2.T, extent=extent, norm=norm, origin="lower", aspect="auto")
        plt.colorbar(surf2, ax=ax2, label=_make_ax_label(z_arg), fraction=0.09, pad=0.04)

        ax2.set_title(title)

        fig_ref.tight_layout()

        ax2.set_xlabel(_make_ax_label(x_arg))
        ax2.set_ylabel(_make_ax_label(y_arg))


def test_image_plot_raises_with_gtr2_arr_args():
    out = cc.calculate(
        "FWHM",
        L=np.linspace(1, 10, 11),
        R1=np.linspace(0.01, 0.99, 9),
        R2="R1",
        meshes=True,
    )

    with pytest.raises(CavCalcError, match="Cannot plot results over 3 dimensions"):
        out.plot(show=False)


@pytest.mark.parametrize("cmap", ("foo", "bar", 0, -3.14))
def test_image_plot_raises_with_invalid_cmap(cmap):
    out = cc.calculate(
        "FWHM",
        L=1,
        R1=np.linspace(0.01, 0.99, 9),
        R2="R1",
        meshes=True,
    )

    with pytest.raises(CavCalcError):
        out.plot(show=False, cmap=cmap)


def test_image_plot_raises_with_multi_x_arr_args():
    out = cc.calculate(
        "w1",
        L="10km",
        wl=np.linspace(1e3, 2e3, 20),
        g1=np.linspace(-1.4, -0.5, 20),
        g2="g1",
        meshes="g1,g2",
    )

    with pytest.raises(CavCalcError, match="Image plots with multiple x-axes are not supported"):
        out.plot(show=False)
