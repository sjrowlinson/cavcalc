"""Check that setting arguments to reference other parameters works as expected."""

import cavcalc as cc
from cavcalc.errors import CavCalcError
import pytest
import numpy as np


@pytest.mark.parametrize("g1g2", ((0.5, "g1"), (-0.8, "g1"), ("g2", 0.99), ("g2", -1e-6)))
def test_referencing_dimensionless(g1g2):
    g1, g2 = g1g2
    out = cc.calculate(L=1, g1=g1, g2=g2)

    assert out.given["g1"].value == out.given["g2"].value


@pytest.mark.parametrize("Rc1Rc2", (("5cm", "Rc1"), ("Rc2", 3), ("Rc2", "3.45km")))
def test_referencing_with_units(Rc1Rc2):
    Rc1, Rc2 = Rc1Rc2
    out = cc.calculate(L="1cm", Rc1=Rc1, Rc2=Rc2)

    assert out.given["Rc1"].value == out.given["Rc2"].value


@pytest.mark.parametrize("Rc1Rc2", (("L", "Rc1"), ("Rc2", "L")))
def test_reference_chain(Rc1Rc2):
    Rc1, Rc2 = Rc1Rc2
    out = cc.calculate("gouy", L=1, Rc1=Rc1, Rc2=Rc2)

    assert out.given["Rc1"].value == out.given["L"].value
    assert out.given["Rc2"].value == out.given["Rc1"].value


@pytest.mark.parametrize(
    "Rc1Rc2", ((np.linspace(10, 20, 11), "Rc1"), ("Rc2", np.linspace(40, 80, 20)))
)
@pytest.mark.parametrize("meshes", (True, False))
def test_referencing_array_parameter(Rc1Rc2, meshes):
    Rc1, Rc2 = Rc1Rc2
    out = cc.calculate("w0", L=12, Rc1=Rc1, Rc2=Rc2, meshes=meshes)

    assert np.all(out.given["Rc1"].value.m == out.given["Rc1"].value.m)


def test_input_as_existing_param():
    out = cc.calculate(L="23cm", Rc="25cm")
    out2 = cc.calculate(L=out.given["L"], gouy=out["gouy"])

    assert out2.given["L"].value.m == pytest.approx(out.given["L"].value.m)
    assert out2["Rc"].value.to("cm").m == pytest.approx(out.given["Rc"].value.m)
    assert out2.given["gouy"].value.m == pytest.approx(out["gouy"].value.m)


@pytest.mark.parametrize("g1g2", ((0.5, "g2"), ("g1", 0.5)))
def test_self_referencing_raises(g1g2):
    g1, g2 = g1g2
    with pytest.raises(CavCalcError, match="Argument.*references itself"):
        cc.calculate(g1=g1, g2=g2)


def test_referencing_nonexistent_raises():
    with pytest.raises(CavCalcError, match="Argument 'Rc2' references.*not given"):
        cc.calculate(L=1, Rc2="Rc1", R1=0.99, R2=0.999)


def test_reference_loop_raises():
    with pytest.raises(CavCalcError, match="Encountered a reference loop.*parameter 'g1'"):
        cc.calculate(g1="g2", g2="g1")


@pytest.mark.parametrize(
    "args",
    (
        {"L": "101um", "div": "L"},
        {"L": "gouy", "gouy": "30deg"},
        {"L": "4km", "R1": 0.99, "R2": "L"},
    ),
)
def test_unit_mismatch_reference_raises(args):
    with pytest.raises(CavCalcError, match="Invalid units.*for parameter"):
        cc.calculate(**args)
