import cavcalc as cc
import pytest


@pytest.mark.parametrize(
    "args",
    (
        ("FSR", {"L": 10.4}),
        ("w0", {"Rc1": "34cm", "Rc2": "38cm", "L": "10cm"}),
        (None, {"L": "4km", "R1": 0.99, "R2": 0.999, "L1": 1e-5, "L2": "L1"}),
    ),
)
def test_output_print_method(args, capsys):
    target, kwargs = args
    out = cc.calculate(target, **kwargs)
    out.print()

    assert capsys.readouterr().out == (str(out) + "\n")
