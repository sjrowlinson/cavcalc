"""Test that ArgParameters get appropriately marked as used."""

import cavcalc as cc
import pytest


@pytest.mark.parametrize(
    "args",
    (
        ("w0", {"L": 10, "gs": -0.4}),
        ("w", {"L": "33cm", "Rc": "69cm", "wl": "1550nm"}),
        ("w2", {"L": "4km", "Rc1": 1934, "g2": -0.7}),
    ),
)
def test_single_target_all_parameters_used(args):
    target, kwargs = args
    out = cc.calculate(target, **kwargs)

    assert all(arg_p.was_used for arg_p in out.given.values())


@pytest.mark.parametrize(
    "args",
    (
        {"L": 10, "gs": -0.4},
        {"L": "33cm", "Rc": "69cm", "wl": "1550nm"},
        {"L": "4km", "Rc1": 1934, "g2": -0.7},
    ),
)
def test_all_target_all_parameters_used(args):
    out = cc.calculate(**args)

    assert all(arg_p.was_used for arg_p in out.given.values())


@pytest.mark.parametrize(
    "args",
    (
        ("gouy", {"gs": 0.9}),
        ("gouy", {"g1": -0.6, "g2": -0.9}),
        ("FSR", {"L": "23.3cm"}),
        ("Aext", {"T1": 0.01, "T2": 0.005}),
    ),
)
def test_single_target_only_wavelength_unused(args):
    target, kwargs = args
    out = cc.calculate(target, **kwargs)

    assert not out.given["wl"].was_used
    assert all(arg_p.was_used for arg_p in filter(lambda p: p.name != "wl", out.given.values()))


@pytest.mark.parametrize(
    "args",
    (
        {"gs": 0.9},
        {"L": "23.3cm", "R1": 0.9, "R2": 0.98},
        {"T1": 0.01, "L": 33},
    ),
)
def test_all_target_only_wavelength_unused(args):
    out = cc.calculate(**args)

    assert not out.given["wl"].was_used
    assert all(arg_p.was_used for arg_p in filter(lambda p: p.name != "wl", out.given.values()))


@pytest.mark.parametrize(
    "args",
    (
        ("Aint", {"R1": 0.9, "R2": 0.99}),
        ("FWHM", {"L": "4.5um", "R1": 0.8, "R2": 0.77}),
        ("Atrn", {"R1": 0.99, "R2": 0.999}),
    ),
)
def test_single_target_only_defaults_unused(args):
    target, kwargs = args
    out = cc.calculate(target, **kwargs)

    expect_unused = ("wl", "L1", "L2")

    for p in expect_unused:
        assert not out.given[p].was_used
    assert all(
        arg_p.was_used
        for arg_p in filter(lambda p: p.name not in expect_unused, out.given.values())
    )


def test_gouy_target_length_unused_when_g_given():
    out = cc.calculate("gouy", L=1, g=0.7)

    assert out.given["g"].was_used
    assert not out.given["L"].was_used


def test_gouy_target_length_curv_used_when_g_not_given():
    out = cc.calculate("gouy", L=1, Rc=2.7)

    assert out.given["L"].was_used
    assert out.given["Rc"].was_used
