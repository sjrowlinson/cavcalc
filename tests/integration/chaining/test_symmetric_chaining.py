"""Target chaining tests for symmetric cavity geometric properties."""

import cavcalc as cc
from cavcalc.output import SingleOutput, MultiOutput
import numpy as np
import pytest

# NOTE (sjr) These tests do *not* check for physical correctness of the results, they
#            instead solely test for the right parameters being present, and able to
#            be calculated, as a result of chaining physical arguments. See the
#            test-files in tests/validation for checks on correct results.

### Single target ###


@pytest.mark.parametrize(
    "args",
    (
        {"L": 1, "Rc": 2.4},
        {"L": "10mm", "gouy": 100, "wl": "2um"},
        {"L": "0.25km", "div": "23urad"},
        {"L": 1e-4, "g": 0.78},
    ),
)
def test_w_chains(args):
    "Check that we can compute 'w' indirectly."
    out = cc.calculate("w", **args)

    assert isinstance(out, SingleOutput)

    assert not np.any(np.isnan(out.result.value.m))


@pytest.mark.parametrize(
    "args",
    (
        {"L": 11, "Rc": 45.5},
        {"L": "10km", "gouy": 295},
        {"L": "0.1km", "div": "3.14arcsec"},
        {"L": 0.34, "g": 0.34, "wl": 1550},
        {"L": "4cm", "w": "130um"},
    ),
)
def test_w0_chains(args):
    "Check that we can compute 'w0' indirectly."
    out = cc.calculate("w0", **args)

    assert isinstance(out, SingleOutput)

    assert not np.any(np.isnan(out.result.value.m))


@pytest.mark.parametrize(
    "args",
    (
        {"L": "300um", "w": "67um", "wl": "800nm"},
        {"L": "4km", "gouy": 123},
        {"L": 25, "div": "22mdeg"},
        {"L": "40km", "g": 0.9999},
    ),
)
def test_roc_chains(args):
    "Check that we can compute 'Rc' indirectly."
    out = cc.calculate("Rc", **args)

    assert isinstance(out, SingleOutput)

    assert not np.any(np.isnan(out.result.value.m))


@pytest.mark.parametrize(
    "args",
    (
        {"L": 1, "Rc": 13, "wl": 1111},
        {"L": "3.4in", "w": "1mft"},
        {"gouy": 45.67},
        {"L": "0.8pc", "div": "10narcmin"},
    ),
)
def test_gcav_chains(args):
    "Check that we can compute 'g' indirectly."
    out = cc.calculate("g", **args)

    assert isinstance(out, SingleOutput)

    assert not np.any(np.isnan(out.result.value.m))


@pytest.mark.parametrize(
    "args",
    (
        {"L": "1mm", "Rc": "14.4mm"},
        {"L": "5ft", "w": "0.1in"},
        {"g": 0.5},
        {"L": "1nm", "div": 89},
    ),
)
def test_gouy_chains(args):
    "Check that we can compute 'gouy' indirectly."
    out = cc.calculate("gouy", **args)

    assert isinstance(out, SingleOutput)

    assert not np.any(np.isnan(out.result.value.m))


@pytest.mark.parametrize(
    "args",
    (
        {"L": "19cm", "Rc": "15cm"},
        {"L": 30, "w": "1.21cm"},
        {"L": 1, "g": 0.6},
        {"L": "5yd", "gouy": 304},
    ),
)
def test_divergence_chains(args):
    "Check that we can compute 'div' indirectly."
    out = cc.calculate("div", **args)

    assert isinstance(out, SingleOutput)

    assert not np.any(np.isnan(out.result.value.m))


@pytest.mark.parametrize(
    "args",
    (
        {"L": "93m", "Rc": "0.25km"},
        {"L": 0.765, "w": "0.1cm"},
        {"L": "13ft", "g": 0.6, "wl": 1990},
        {"L": 10, "gouy": 3},
    ),
)
def test_modesep_chains(args):
    "Check that we can compute 'modesep' indirectly."
    out = cc.calculate("modesep", **args)

    assert isinstance(out, SingleOutput)

    assert not np.any(np.isnan(out.result.value.m))


### All target ###


@pytest.mark.parametrize(
    "args",
    (
        {"L": 11, "Rc": 45.5},
        {"L": "300um", "w": "67um", "wl": "800nm"},
        {"L": "4km", "gouy": 123},
        {"L": 25, "div": "22mdeg"},
        {"L": 1, "gs": -0.81, "wl": "2.04um"},
        {"L": "40km", "g": 0.9999},
    ),
)
def test_symm_all_target_chains(args):
    """Check that we get all the expected output parameters for a symmetric cavity
    with different physical argument combinations."""
    out = cc.calculate(**args)

    assert isinstance(out, MultiOutput)

    expected_entries = ["FSR", "modesep", "z0", "Rc", "w", "w0", "g", "gs", "gouy", "div"]
    for given in args:
        if given in expected_entries:
            expected_entries.remove(given)

    assert not (set(expected_entries) - set(out.results.keys()))

    for v in out.results.values():
        assert not np.any(np.isnan(v.value.m))


@pytest.mark.parametrize(
    "args",
    (
        {"gs": 0.44},
        {"gouy": 243.432},
        {"g": 0.667},
    ),
)
def test_symm_all_target_chains_no_cavity_length(args):
    out = cc.calculate(**args)

    assert isinstance(out, MultiOutput)

    expected_entries = ["gouy", "gs", "g"]
    for given in args:
        if given in expected_entries:
            expected_entries.remove(given)

    assert not (set(expected_entries) - set(out.results.keys()))

    for v in out.results.values():
        assert not np.any(np.isnan(v.value.m))
