"""Target chaining tests for non-symmetric cavity geometric properties."""

import cavcalc as cc
from cavcalc.output import SingleOutput, MultiOutput
import numpy as np
import pytest

# NOTE (sjr) These tests do *not* check for physical correctness of the results, they
#            instead solely test for the right parameters being present, and able to
#            be calculated, as a result of chaining physical arguments. See the
#            test-files in tests/validation for checks on correct results.

### Single target ###


@pytest.mark.parametrize("target", ("w1", "w2", "w0", "modesep", "gouy", "div"))
@pytest.mark.parametrize(
    "args",
    (
        {"L": 1, "Rc1": 2.4, "Rc2": 3.2},
        {"L": "10mm", "g1": -0.67, "wl": "2um", "Rc2": "4.4mm"},
        {"L": "0.25km", "Rc1": "0.33km", "g2": 0.1},
    ),
)
def test_w1_w2_w0_modesep_chains(target, args):
    "Check that we can compute 'w1', 'w2', 'w0', 'modesep', 'gouy', and 'div' indirectly."
    out = cc.calculate(target, **args)

    assert isinstance(out, SingleOutput)

    assert not np.any(np.isnan(out.result.value.m))


### All target ###


@pytest.mark.parametrize(
    "args",
    (
        {"L": 11, "Rc1": 45.5, "Rc2": 67.7},
        {"L": "10mm", "g1": -0.67, "wl": "2um", "Rc2": "4.4mm"},
        {"L": "0.25km", "Rc1": "0.33km", "g2": 0.1},
        {"L": "4km", "w1": "52mm", "w2": "63mm"},
    ),
)
def test_nonsymm_all_target_chains(args):
    """Check that we get all the expected output parameters for a non-symmetric cavity
    with different physical argument combinations."""
    out = cc.calculate(**args)

    assert isinstance(out, MultiOutput)

    expected_entries = [
        "FSR",
        "modesep",
        "z0",
        "Rc1",
        "Rc2",
        "w1",
        "w2",
        "w0",
        "g1",
        "g2",
        "g",
        "gouy",
        "div",
    ]
    for given in args:
        if given in expected_entries:
            expected_entries.remove(given)

    assert not (set(expected_entries) - set(out.results.keys()))

    for v in out.results.values():
        assert not np.any(np.isnan(v.value.m))
