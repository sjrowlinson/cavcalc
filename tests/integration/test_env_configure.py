import cavcalc as cc
from cavcalc.parameters import ParameterCategory, valid_arguments
from cavcalc.parameters.tools import get_names
import pytest


@pytest.mark.parametrize(
    "overrides",
    (
        {"w": "um"},
        {"L": "km", "Rc": "km"},
        {"FSR": "GHz", "gouy": "rad"},
        {"w0": "nm", "L": "yd", "div": "arcsec"},
    ),
)
def test_context_configure_override_specific_units(overrides):
    with cc.configure(**overrides):
        out = cc.calculate(L=1, Rc=2.4)

        for p, p_units in overrides.items():  # check that the overrides worked...
            if arg_p := out.given.get(p):
                assert arg_p.value.units == cc.ureg.Unit(p_units)
            elif tgt_p := out.results.get(p):
                assert tgt_p.value.units == cc.ureg.Unit(p_units)

    out = cc.calculate(L=1, Rc=2.4)
    for p, p_units in overrides.items():  # and that we reset correctly on exit from with block
        if arg_p := out.given.get(p):
            assert arg_p.value.units == cc.ureg.Unit(cc.get_default_units(p))
        elif tgt_p := out.results.get(p):
            assert tgt_p.value.units == cc.ureg.Unit(cc.get_default_units(p))


_parents_map = {
    "div": "angles",
    "w": "beamsizes",
    "w1": "beamsizes",
    "w2": "beamsizes",
    "w0": "beamsizes",
    "Rc": "rocs",
    "Rc1": "rocs",
    "Rc2": "rocs",
    "L": "distances",
    "z0": "distances",
    "FSR": "frequencies",
    "FWHM": "frequencies",
    "modesep": "frequencies",
    "pole": "frequencies",
    "gouy": "phases",
    "wl": "waves",
}


@pytest.mark.parametrize(
    "overrides",
    (
        {"distances": "mm"},
        {"rocs": "cm", "frequencies": "GHz"},
        {"phases": "rad", "angles": "arcmin", "beamsizes": "um"},
    ),
)
def test_context_configure_override_categories(overrides):
    with cc.configure(**overrides):
        out = cc.calculate(L=1, Rc=2.4)

        for tgt_p in out.results.values():
            if not tgt_p.is_unitless:
                expect_units = overrides.get(_parents_map[tgt_p.name])
                if expect_units:
                    assert tgt_p.value.units == cc.ureg.Unit(expect_units)
                else:
                    assert tgt_p.value.units == cc.ureg.Unit(cc.get_default_units(tgt_p.name))

        for arg_p in out.given.values():
            if not arg_p.is_unitless:
                expect_units = overrides.get(_parents_map[arg_p.name])
                if expect_units:
                    assert arg_p.value.units == cc.ureg.Unit(expect_units)
                else:
                    assert arg_p.value.units == cc.ureg.Unit(cc.get_default_units(arg_p.name))


@pytest.mark.parametrize("pname", get_names(ParameterCategory.Power, ParameterCategory.Stability))
def test_dimensionless_param_unit_override_warns(pname):
    with pytest.warns(UserWarning, match="Ignoring units override for dimensionless"):
        with cc.configure(**{pname: "cm"}):
            if pname in valid_arguments:
                out = cc.calculate(**{pname: 0.5})

                assert out.given[pname].value.m == pytest.approx(0.5)
                assert out.given[pname].value.unitless


@pytest.mark.parametrize("pname", ("foo", "bar", "L_", "_Rc"))
def test_invalid_param_unit_override_warns(pname):
    with pytest.warns(UserWarning, match="Ignoring units override for invalid"):
        with cc.configure(**{pname: "cm"}):
            if pname in valid_arguments:
                out = cc.calculate(**{pname: 0.5})

                assert out.given[pname].value.m == pytest.approx(0.5)
                assert out.given[pname].value.unitless
