"""Test mesh argument ordering via the CLI."""

from cavcalc.errors import CavCalcError
from cavcalc.testing import cli_main
import pytest


@pytest.mark.parametrize("mesh", ("True", "g1,g2", "g2,g1", "False"))
def test_mesh_ordering(mesh):
    out = cli_main(["gouy", "-g1", "-1 1 9", "-g2", "-1 1 9", "--mesh", mesh])

    if mesh == "False":
        assert out.given["g1"].axis == 0
        assert out.given["g2"].axis == 0
        assert out.given["g1"].index == 0
        assert out.given["g2"].index == 1
        assert out.result.value.m.shape == (9,)
    else:
        assert out.result.value.m.shape == (9, 9)

        if mesh == "True":
            assert out.given["g1"].axis == 0
            assert out.given["g2"].axis == 1
        else:
            gn, gm = mesh.split(",")
            assert out.given[gn].axis == 0
            assert out.given[gm].axis == 1


@pytest.mark.parametrize("mesh", ("R1,R2", "L,gs", "Rc,gouy", "L,Rc,div"))
def test_unspecified_mesh_args(mesh):
    with pytest.raises(CavCalcError, match=r"Mesh parameter.*was not specified"):
        cli_main(["-L", "1 2 3", "-Rc", "4 5 3", "--mesh", mesh])


@pytest.mark.parametrize("mesh", ("gouy,L", "L,gouy"))
def test_mesh_arg_not_array(mesh):
    with pytest.raises(CavCalcError, match=r"One or more.*is not array-like"):
        cli_main(["-L", "4km", "-gouy", "312", "--mesh", mesh])


@pytest.mark.parametrize("mesh", ("L,Rc,L", "Rc, L, Rc, Rc"))
def test_mesh_invalid_repeated_internal(mesh):
    with pytest.raises(CavCalcError, match=r"Parameter.*was given \d+ times"):
        cli_main(["-L", "1 2 3", "-Rc", "4 5 3", "--mesh", mesh])


def test_mesh_invalid_repeated_external():
    with pytest.raises(CavCalcError, match=r"Parameter.*was already specified in a previous mesh"):
        cli_main(
            [
                "-L",
                "1 2 3",
                "-Rc",
                "4 5 3",
                "-R1",
                "0.8 0.9 3",
                "-R2",
                "0.9 0.99 3",
                "--mesh",
                "L,Rc;L,R1,R2",
            ]
        )
