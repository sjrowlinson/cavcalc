from cavcalc.errors import CavCalcError
from cavcalc.testing import cli_main

import numpy as np
import pytest


@pytest.mark.parametrize(
    "args",
    (
        {"L": 10, "Rc": np.linspace(8, 12, 49)},
        {"gouy": np.linspace(20, 160, 80)},
        {"L": "33.3 cm", "w1": np.linspace(1, 60, 30), "w2": np.linspace(1, 80, 30)},
    ),
)
@pytest.mark.parametrize("save_type", ("binary", "txt"))
def test_load_data_as_physical_arg(args, save_type, tmp_path):
    """Check that specifying a physical argument as a file correctly loads
    this file into the values of this parameter."""
    arr_args = {k: v for k, v in args.items() if isinstance(v, np.ndarray)}

    for pname, values in arr_args.items():
        if save_type == "binary":
            np.save(tmp_path / f"data_{pname}", values)
        else:
            np.savetxt(tmp_path / f"data_{pname}", values)

    ext = ".npy" if save_type == "binary" else ""
    command = []
    for pname, v in args.items():
        command.append(f"-{pname}")
        if pname in arr_args:
            command.append(str(tmp_path / f"data_{pname}{ext}"))
        else:
            command.append(str(v))

    out = cli_main(command)

    for pname, values in arr_args.items():
        assert np.all(out.given[pname].value.m == values)


def test_load_non_numeric_data_raises(tmp_path):
    afile = str(tmp_path / "str_arr_file.npy")
    str_array = np.array(["10cm", "12cm", "13mm"])

    np.save(afile, str_array)

    with pytest.raises(CavCalcError, match="Invalid data-type"):
        cli_main(["-L", afile])


def test_load_complex_data_raises(tmp_path):
    afile = str(tmp_path / "cmplx_arr_file.npy")
    complex_array = np.ones(5, dtype=np.complex128)

    np.save(afile, complex_array)

    with pytest.raises(CavCalcError, match="Invalid data-type"):
        cli_main(["-L", afile])


def test_load_object_data_raises(tmp_path):
    afile = str(tmp_path / "obj_arr_file.npy")
    obj_array = np.arange(10, dtype="O")

    np.save(afile, obj_array)

    with pytest.raises(CavCalcError, match="Unable to load file.*Object arrays cannot be loaded"):
        cli_main(["-L", afile])
