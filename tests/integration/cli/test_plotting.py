from cavcalc.errors import CavCalcError
from cavcalc.testing import cli_main
from cavcalc.output import SingleOutput
import pytest


@pytest.mark.parametrize("plot", ("--plot", "--logxplot", "--logyplot", "--logplot"))
@pytest.mark.parametrize("plot_opts", (None, "color: k", "marker: x, linewidth: 0"))
def test_line_plot_single_target(plot, plot_opts):
    import matplotlib.pyplot as plt

    if plot_opts is not None:
        out = cli_main(["FSR", "-L", "1 10 9", plot, "--plot-opts", plot_opts, "--no-plot-display"])
    else:
        out = cli_main(["FSR", "-L", "1 10 9", plot, "--no-plot-display"])

    assert isinstance(out, SingleOutput)
    plt.close()


@pytest.mark.parametrize("plot_opts", ("color k", "marker: x, linewidth 0", "foo: bar"))
@pytest.mark.parametrize(
    "command",
    (
        ["FSR", "-L", "1 10 9"],
        ["gouy", "-g1", "-2 2 11", "-g2", "g1", "--mesh", "true"],
        ["w0", "-L", "3cm", "-w1", "80 150 19 um", "-w2", "w1", "--mesh", "true"],
    ),
)
def test_invalid_plot_opts_raises(plot_opts, command):
    with pytest.raises(CavCalcError):
        cli_main(command + ["--plot", "--plot-opts", plot_opts, "--no-plot-display"])


def test_image_plot_single_target():
    out = cli_main(
        ["gouy", "-g1", "-2 2 11", "-g2", "g1", "--mesh", "true", "--plot", "--no-plot-display"]
    )
    assert isinstance(out, SingleOutput)
