"""Check that by specifying output units via the CLI we get the expected
units in the resulting Output object."""

from cavcalc import ureg
from cavcalc.errors import CavCalcError
from cavcalc.testing import cli_main
import pytest


@pytest.mark.parametrize("target", ("FSR", "FWHM", "pole"))
@pytest.mark.parametrize("units", ("GHz", "nHz", "1/hr", "Hz"))
def test_specified_frequency_output_units(target, units):
    out = cli_main([target, "-L", "1", "-R1", "0.9", "-R2", "0.9", "-u", units])
    assert out.result.value.units == ureg.Unit(units)


@pytest.mark.parametrize("target", ("w", "w0"))
@pytest.mark.parametrize("units", ("cm", "mm", "yd", "ly", "in"))
def test_specified_beamsize_output_units(target, units):
    out = cli_main([target, "-L", "1", "-gs", "-0.9", "-u", units])
    assert out.result.value.units == ureg.Unit(units)


@pytest.mark.parametrize("target", ("gouy", "div"))
@pytest.mark.parametrize("units", ("deg", "rad", "degrees", "radians", "arcsec"))
def test_specified_angle_phase_output_units(target, units):
    out = cli_main([target, "-L", "1", "-Rc", "3.3", "-u", units])
    assert out.result.value.units == ureg.Unit(units)


@pytest.mark.parametrize(
    "target_with_units", (("w", "Hz"), ("modesep", "mm"), ("z0", "deg"), ("gouy", "hr"))
)
def test_incompatible_specified_output_units(target_with_units):
    target, units = target_with_units
    with pytest.raises(CavCalcError, match="Cannot convert"):
        cli_main([target, "-L", "10mm", "-gs", "-0.5", "-u", units])


@pytest.mark.parametrize("target_with_units", (("g", "um"), ("gs", "GHz"), ("finesse", "W")))
def test_units_for_dimensionless_target_raises(target_with_units):
    target, units = target_with_units
    with pytest.raises(CavCalcError, match="Units.*given for dimensionless target"):
        cli_main([target, "-L", "13cm", "-Rc", "35cm", "-R1", "0.8", "-R2", "0.9", "-u", units])


@pytest.mark.parametrize("units", ("cm", "GHz", "arcmin"))
def test_units_given_for_multi_target_mode_warns(units):
    with pytest.warns(UserWarning, match="Specifying output units.*only.*single target mode"):
        cli_main(["-L", "45.5mm", "-g1", "-0.7", "-g2", "-0.9", "-u", units])
