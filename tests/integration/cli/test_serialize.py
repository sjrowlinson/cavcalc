"""Tests for saving and loading cavcalc output objects via the CLI. This is
similar to `tests/integration/test_serialize_output.py`, but covers the CLI
specific code for serialization in the main function."""

from cavcalc.errors import CavCalcError
from cavcalc.testing import cli_main
from cavcalc.output import SingleOutput, MultiOutput
import pickle
import pytest


@pytest.mark.parametrize("target", ("w", "w0", "gouy", "div"))
def test_serialize_single_output(target, tmp_path):
    ccfile = str(tmp_path / f"output_{target}.cav")
    out = cli_main([target, "-L", "1", "-gs", "-0.9", "-save", ccfile])

    out_load = cli_main(["-load", ccfile])
    assert isinstance(out_load, SingleOutput)

    assert str(out) == str(out_load)


@pytest.mark.parametrize("target", ("w", "w0", "gouy", "div"))
def test_fail_load_single_output(target, tmp_path):
    ccfile = str(tmp_path / f"output_{target}.cav")
    cli_main([target, "-L", "1", "-gs", "-0.9", "-save", ccfile])

    with pytest.raises(
        CavCalcError, match=r"Mismatch between target parameter.*output object parameter"
    ):
        cli_main(["z0", "-load", ccfile])


def test_serialize_multi_output(tmp_path):
    ccfile = str(tmp_path / "output.cav")
    out = cli_main(["-L", "4km", "-Rc1", "1934", "-Rc2", "2245", "-save", ccfile])

    out_load = cli_main(["-load", ccfile])
    assert isinstance(out_load, MultiOutput)

    assert str(out) == str(out_load)


@pytest.mark.parametrize("target", ("w1", "w2", "w0", "gouy", "modesep"))
def test_load_single_from_multi_output(target, tmp_path):
    ccfile = str(tmp_path / "output.cav")
    out = cli_main(["-L", "4 km", "-Rc1", "1934", "-Rc2", "2245", "-save", ccfile])

    out_load = cli_main([target, "-load", ccfile])
    assert isinstance(out_load, SingleOutput)

    assert str(out.as_single(target)) == str(out_load)


@pytest.mark.parametrize(
    "target_units",
    (
        ("w1", "cm"),
        ("gouy", "rad"),
        ("modesep", "GHz"),
        ("w0", "in"),
    ),
)
def test_load_single_with_target_units(target_units, tmp_path):
    target, units = target_units

    ccfile = str(tmp_path / "output.cav")
    out = cli_main(["-L", "4km", "-Rc1", "1934", "-Rc2", "2245", "-save", ccfile])

    out_load = cli_main([target, "-u", units, "-load", ccfile])
    assert isinstance(out_load, SingleOutput)

    tgt_out = out.as_single(target)
    tgt_out.convert(units)
    assert tgt_out.result.value == out_load.result.value


@pytest.mark.parametrize(
    "target_units",
    (
        ("w1", "foo"),
        ("gouy", "_rad_"),
        ("modesep", "mm"),
        ("w0", "s"),
    ),
)
def test_load_single_with_invalid_target_units_raises(target_units, tmp_path):
    target, units = target_units

    ccfile = str(tmp_path / "output.cav")
    cli_main(["-L", "4km", "-Rc1", "1934", "-Rc2", "2245", "-save", ccfile])

    with pytest.raises(CavCalcError):
        cli_main([target, "-u", units, "-load", ccfile])


@pytest.mark.parametrize("target", ("w1", "w2", "w0", "gouy", "modesep"))
def test_fail_load_single_from_multi_output(target, tmp_path):
    ccfile = str(tmp_path / "output.cav")
    cli_main(["-L", "33cm", "-R1", "0.8", "-R2", "0.9", "-save", ccfile])

    with pytest.raises(CavCalcError, match=r"The target parameter.*is not in the output"):
        cli_main([target, "-load", ccfile])


def test_giving_physical_args_when_loading_raises(tmp_path):
    ccfile = str(tmp_path / "output.cav")
    cli_main(["-L", "33cm", "-R1", "0.8", "-R2", "0.9", "-save", ccfile])

    with pytest.raises(CavCalcError, match="cannot give physical parameter.*loading.*output"):
        cli_main(["-load", ccfile, "-L", "12cm", "-R2", "0.75"])


class _tmp_A:
    pass


def test_loading_non_output_type_raises(tmp_path):
    afile = str(tmp_path / "afile.cav")
    with open(afile, "wb") as f:
        pickle.dump(_tmp_A(), f, pickle.HIGHEST_PROTOCOL)

    with pytest.raises(CavCalcError, match="Unrecognised serialized object"):
        cli_main(["-load", afile])
