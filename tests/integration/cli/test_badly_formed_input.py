from cavcalc.errors import CavCalcError
from cavcalc.testing import cli_main
import pytest


@pytest.mark.parametrize(
    "command",
    (
        ["-L", "1 2 3 4 5", "-gs", "0.6"],
        ["-R1", "0.1 0.9 11", "-R2", "0.1 0.9 11 0.01 0"],
    ),
)
def test_invalid_data_range_syntax_raises(command):
    with pytest.raises(CavCalcError, match="Expected data range in format"):
        cli_main(command)


@pytest.mark.parametrize(
    "command",
    (
        ["-L", "a 10 100 in", "-gs", "0.6"],
        ["-R1", "0.1 0.9 11", "-R2", "0.1y 0.9 11"],
        ["-L", "4km", "-Rc", "_ 2500 50"],
    ),
)
def test_invalid_data_range_start_value_raises(command):
    with pytest.raises(CavCalcError, match="Could not convert.*start.*floating point"):
        cli_main(command)


@pytest.mark.parametrize(
    "command",
    (
        ["-L", "1 c 100", "-gs", "0.6"],
        ["-R1", "0.1 f 11", "-R2", "0.1 0.9 11"],
        ["-L", "4km", "-Rc", "2.1 _0 50 km"],
    ),
)
def test_invalid_data_range_stop_value_raises(command):
    with pytest.raises(CavCalcError, match="Could not convert.*stop.*floating point"):
        cli_main(command)


@pytest.mark.parametrize(
    "command",
    (
        ["-L", "1 inf 100", "-gs", "0.6"],
        ["-R1", "-inf inf 11", "-R2", "0.1 0.9 11"],
        ["-L", "4km", "-Rc", "-inf 100 50 km"],
    ),
)
def test_inf_in_data_range_raises(command):
    with pytest.raises(CavCalcError, match="Encountered 'inf' in a data range"):
        cli_main(command)


@pytest.mark.parametrize(
    "command",
    (
        ["-L", "1 nan 100", "-gs", "0.6"],
        ["-R1", "-nan nan 11", "-R2", "0.1 0.9 11"],
        ["-L", "4km", "-Rc", "-nan 100 50 km"],
    ),
)
def test_nan_in_data_range_raises(command):
    with pytest.raises(CavCalcError, match="Encountered 'NaN' in a data range"):
        cli_main(command)


@pytest.mark.parametrize(
    "command",
    (
        ["-L", "3.4 50 foo cm", "-g1", "-0.4", "-g2", "-0.8"],
        ["-R1", "0.1 0.9 bar", "-R2", "0.5 0.99 100"],
    ),
)
def test_invalid_data_range_num_value_raises(command):
    with pytest.raises(CavCalcError, match="Could not convert.*num.*integer"):
        cli_main(command)


@pytest.mark.parametrize(
    "command",
    (
        ["-L", "1e-5 1e-3 -50", "-gouy", "310"],
        ["-div", "0.01 0.05 -1 rad", "-L", "1km"],
        ["-L", "43cm", "-wl", "1 2 0 um", "gs", "0.4"],
    ),
)
def test_data_range_non_positive_num_raises(command):
    with pytest.raises(CavCalcError, match="Number of points.*must be a positive integer"):
        cli_main(command)


@pytest.mark.parametrize(
    "command",
    (
        ["-L", "4km", "-Rc", "2100 2500"],
        ["-gouy", "45 90"],
    ),
)
def test_invalid_parameter_value_syntax_raises(command):
    with pytest.raises(CavCalcError, match="cannot have.*scaling"):
        cli_main(command)


@pytest.mark.parametrize(
    "command",
    (
        ["-L", "4km", "-Rc", "2100 foo"],
        ["-gouy", "45 _deg"],
    ),
)
def test_invalid_parameter_unit_syntax_raises(command):
    with pytest.raises(CavCalcError, match="not defined.*unit registry"):
        cli_main(command)


@pytest.mark.parametrize("lims", ("0", "-3 3 5", "1 2 3 4 5", "'-3,4'", "'10:20'"))
@pytest.mark.parametrize("which", ("xlim", "ylim", "zlim"))
def test_invalid_plot_lim_syntax(lims, which):
    with pytest.raises(CavCalcError, match='Expected limits specified as "<low> <high>"'):
        cli_main(["-L", "1", "-Rc", "2 30 100", f"--{which}", lims])


@pytest.mark.parametrize("lims", ("-foo 3", "bar 10", "0.1abc None"))
@pytest.mark.parametrize("which", ("xlim", "ylim", "zlim"))
def test_invalid_plot_lim_lower_value(lims, which):
    with pytest.raises(CavCalcError, match="Lower limit.*number, or None"):
        cli_main(["-L", "1", "-Rc", "2 30 100", f"--{which}", lims])


@pytest.mark.parametrize("lims", ("-3 foo", "1 bar", "None 30def"))
@pytest.mark.parametrize("which", ("xlim", "ylim", "zlim"))
def test_invalid_plot_lim_upper_value(lims, which):
    with pytest.raises(CavCalcError, match="Upper limit.*number, or None"):
        cli_main(["-L", "1", "-Rc", "2 30 100", f"--{which}", lims])


@pytest.mark.parametrize(
    "args",
    (
        ["-L", "5foo", "-Rc", "1"],
        ["-gouy", "0.09barr"],
        ["-Rc1", "5cm", "-Rc2", "6.7cm_", "-L", "1cm"],
        ["-L", "1 10 11 blah", "-R1", "0.9", "-R2", "0.98"],
    ),
)
def test_invalid_units_input_raises(args):
    with pytest.raises(CavCalcError, match="not defined in the unit registry"):
        cli_main(args)
