"""Checks that passing in meshes in different ways, via the API, results
in the correct shapes and ordering of corresponding physical arguments."""

import cavcalc as cc
from cavcalc.errors import CavCalcError
from cavcalc.parameters import ArgParameter
import numpy as np
import pytest


@pytest.mark.parametrize(
    "meshes",
    (
        "g1,g2",
        ("g1", "g2"),
        ("g1,g2",),
        True,
        "g1, g2;",
        [
            ("g1", "g2"),
        ],
    ),
)
def test_valid_mesh_arg_styles(meshes):
    out = cc.calculate("gouy", meshes=meshes, g1=np.linspace(-1, 1, 3), g2=np.linspace(-1, 1, 3))

    assert out.result.value.m.shape == (3, 3)

    assert out.given["g1"].axis == 0
    assert out.given["g2"].axis == 1


def test_false_mesh():
    out = cc.calculate("w", meshes=False, L=np.linspace(1, 10, 5), Rc=np.linspace(5, 50, 5))

    assert out.result.value.m.shape == (5,)

    assert out.given["L"].axis == 0
    assert out.given["L"].index == 0
    assert out.given["Rc"].axis == 0
    assert out.given["Rc"].index == 1


def test_automesh_with_no_array_args_warns():
    with pytest.warns(UserWarning, match="Ignoring mesh.*no parameters are array-like"):
        out = cc.calculate("Aint", meshes=True, R1=0.9, R2=0.99)

        assert out.given["R1"].is_scalar
        assert out.given["R2"].is_scalar
        assert out.result.is_scalar


@pytest.mark.parametrize(
    "args",
    (
        ("FWHM", {"L": 1, "R1": np.linspace(0.1, 0.9), "R2": 0.8, "meshes": True}),
        ("w0", {"L": "4km", "Rc1": 1934, "Rc2": np.linspace(2100, 2400), "meshes": "Rc2"}),
        ("FSR", {"L": np.logspace(1, 3), "meshes": True}),
    ),
)
def test_mesh_with_single_array_arg_warns(args):
    target, kwargs = args
    with pytest.warns(UserWarning, match="Ignoring single parameter.*in mesh"):
        out = cc.calculate(target, **kwargs)

        assert out.result.value.m.shape == (50,)


@pytest.mark.parametrize("meshes", (1, np.inf, ArgParameter("g1")))
def test_invalid_mesh_type_not_iterable(meshes):
    with pytest.raises(CavCalcError, match="Expected this argument to be iterable"):
        cc.calculate("gouy", meshes=meshes, g1=np.linspace(-1, 1, 3), g2=np.linspace(-1, 1, 3))


@pytest.mark.parametrize("meshes", (["g1", ("g2",)], ("g2", ["g1,g2"])))
def test_invalid_mesh_type_bad_string(meshes):
    with pytest.raises(
        CavCalcError, match="single parameter names, or.*combination.*separated by commas"
    ):
        cc.calculate("gouy", meshes=meshes, g1=np.linspace(-1, 1, 3), g2=np.linspace(-1, 1, 3))


@pytest.mark.parametrize(
    "meshes",
    (
        [
            1,
        ],
        ("g1,g2", 0.5),
    ),
)
def test_invalid_mesh_type_inner_not_iterable(meshes):
    with pytest.raises(
        CavCalcError, match="parameter name combinations.*iterable of parameter names"
    ):
        cc.calculate("gouy", meshes=meshes, g1=np.linspace(-1, 1, 3), g2=np.linspace(-1, 1, 3))
