"""Tests for saving and loading cavcalc output objects."""

import cavcalc as cc
from cavcalc.output import SingleOutput, MultiOutput
import numpy as np
import pytest


@pytest.mark.parametrize("target", ("w", "w0", "gouy", "div"))
@pytest.mark.parametrize(
    "args",
    (
        {"L": 1, "Rc": 3.14},
        {"L": "3cm", "gs": -0.9},
        {"L": "10km", "g": 0.8},
        {"L": 10, "Rc": np.linspace(8, 12, 49)},
    ),
)
def test_save_and_load_single_output(target, args, tmp_path):
    out = cc.calculate(target, **args)

    ccfile = tmp_path / f"output_{target}.cav"
    out.save(ccfile)

    out_load = cc.load(ccfile)
    assert isinstance(out_load, SingleOutput)

    assert str(out) == str(out_load)


@pytest.mark.parametrize(
    "args",
    (
        {"L": "4km", "Rc1": 1934, "Rc2": 2245, "R1": 0.99, "R2": 0.999},
        {"L": "3cm", "gs": -0.9, "wl": "1550nm"},
        {"L": 10, "Rc": np.linspace(8, 12, 49)},
    ),
)
def test_save_and_load_multi_output(args, tmp_path):
    out = cc.calculate(**args)

    ccfile = tmp_path / f"output.cav"
    out.save(ccfile)

    out_load = cc.load(ccfile)
    assert isinstance(out_load, MultiOutput)

    assert str(out) == str(out_load)
