"""Check that giving non-physical input values correctly raises errors with appropriate messages."""

import cavcalc as cc
from cavcalc.errors import CavCalcError
from cavcalc.parameters import valid_targets
from cavcalc.parameters.tools import get_names, get_type
import numpy as np
import pytest


@pytest.mark.parametrize("L_val", (-1, "-23km", np.linspace(-1e-4, 10, 30)))
def test_negative_cavity_length_raises(L_val):
    with pytest.raises(CavCalcError, match="Cavity lengths must be non-negative"):
        cc.calculate(L=L_val)


@pytest.mark.parametrize("wl_val", (0, "-1.5um", np.linspace(-1e-12, 1e-9, 19)))
def test_non_positive_wavelength_raises(wl_val):
    with pytest.raises(CavCalcError, match="Wavelengths must be positive"):
        cc.calculate(wl=wl_val)


@pytest.mark.parametrize("w_arg", ("w", "w1", "w2"))
@pytest.mark.parametrize("w_val", (-1, "-12.4in", np.linspace(-3, 3, 5)))
def test_negative_beamsize_raises(w_arg, w_val):
    with pytest.raises(CavCalcError, match="Beam sizes must be non-negative"):
        cc.calculate(**{w_arg: w_val})


@pytest.mark.parametrize("arg", ("R1", "T1", "L1", "R2", "T2", "L2"))
@pytest.mark.parametrize("val", (-0.5, 2.3, np.linspace(-1, 1, 3), np.linspace(0.5, 1.4, 5)))
def test_RTL_out_of_bounds_raises(arg, val):
    with pytest.raises(CavCalcError, match=f"{arg} is invalid.*must satisfy 0 <= {arg[0]} <= 1"):
        cc.calculate(**{arg: val})


@pytest.mark.parametrize(
    "args",
    (
        {"R1": 0.9, "T1": 0.1},
        {"T2": 0.03, "R2": 0.9},
        {"L1": 1e-5, "R1": 0.1, "T1": 0.85},
        {"R1": 0.9, "R2": "R1", "L1": 1e-3, "T2": "L1"},
    ),
)
def test_both_R_and_T_given_raises(args):
    with pytest.raises(CavCalcError, match="Cannot specify both reflectivity and transmission"):
        cc.calculate(**args)


@pytest.mark.parametrize(
    "args",
    (
        {"R1": 0.9, "L1": 0.2},
        {"L2": 0.999, "T2": 0.01},
    ),
)
def test_RL_or_TL_breaks_constraint_raises(args):
    with pytest.raises(CavCalcError, match="must satisfy.*\+ L <= 1"):
        cc.calculate(**args)


def test_illogical_stability_combo_symm():
    with pytest.raises(CavCalcError, match="Cavity g-factor and symmetric.*cannot be specified"):
        cc.calculate(g=0.7, gs=0.3)


def test_illogical_stability_combo_asymm():
    with pytest.raises(CavCalcError, match="Cavity g-factor and individual.*cannot be specified"):
        cc.calculate(g=0.44, g1=-0.3, g2=-0.99)


def test_illogical_stability_combo_symm_and_asymm():
    with pytest.raises(CavCalcError, match="Individual.*and symmetric.*cannot be specified"):
        cc.calculate(gs=-0.87, g1=-0.4, g2=-0.6)


def test_illogical_curvature_combo():
    with pytest.raises(
        CavCalcError, match="Symmetric curvature.*and individual.*cannot be specified"
    ):
        cc.calculate(Rc="3cm", Rc1="10cm", Rc2="1cm")


def test_illogical_beamsize_combo():
    with pytest.raises(
        CavCalcError, match="Symmetric beam-size.*and individual.*cannot be specified"
    ):
        cc.calculate(w="13mm", w1="9cm", Rc2="100um")


@pytest.mark.parametrize(
    "args",
    (
        {"L": 1, "Rc": 2.4, "gouy": 310},
        {"L": "67cm", "Rc1": "150cm", "Rc2": "110cm", "w1": "45um", "w2": "56um"},
        {"L": 78, "Rc": 190, "div": "1mdeg"},
    ),
)
def test_incompatible_parameter_combos_raises(args):
    with pytest.raises(
        CavCalcError, match="Incompatible arguments.*parameter.*can be computed multiple"
    ):
        cc.calculate(**args)


@pytest.mark.parametrize(
    "args",
    (
        ("Atrn", {"R1": np.linspace(0.1, 0.9, 11), "R2": np.linspace(0.1, 0.9, 9)}),
        ("w0", {"L": "4km", "Rc": np.linspace(2100, 2500, 49), "wl": np.linspace(1064, 2050, 100)}),
        (
            "FWHM",
            {
                "L": np.linspace(1, 10, 9),
                "R1": np.linspace(0.3, 0.9, 3),
                "R2": np.linspace(0.1, 0.9, 11),
                "meshes": "L,R2",
            },
        ),
    ),
)
def test_inconsistent_parameter_array_shapes(args):
    target, kwargs = args
    with pytest.raises(CavCalcError, match="Encountered inconsistent parameter shapes"):
        cc.calculate(target, **kwargs)


@pytest.mark.parametrize(
    "target", tuple(get_type(p) for p in set(get_names()).difference(valid_targets))
)
def test_invalid_target_parameters(target):
    with pytest.raises(CavCalcError, match="Invalid target"):
        cc.calculate(target)


@pytest.mark.parametrize(
    "target", tuple(set(get_names()).difference(valid_targets)) + ("foo", "bar")
)
def test_invalid_unrecognised_target_parameters(target):
    with pytest.raises(CavCalcError, match="Unrecognised / invalid target"):
        cc.calculate(target)


@pytest.mark.parametrize("arg", ("foo", "bar", "Ll", "Goouy", "0Rc1"))
def test_invalid_unrecognised_arg_parameters(arg):
    with pytest.raises(CavCalcError, match="Unrecognised / invalid argument"):
        cc.calculate(**{arg: 1})


@pytest.mark.parametrize("arg", ((33, 12, "cm"), (1, 2, 3), (1e-5,), (9.5, "um", "cm", 5.6)))
def test_incorrect_tuple_arg_length_raises(arg):
    with pytest.raises(CavCalcError, match="Expected tuple of length 2 for argument"):
        cc.calculate(L=arg)
