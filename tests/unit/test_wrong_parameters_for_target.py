"""Check that, when giving the wrong set of arguments for a single
target, an appropriate error is raised."""

import cavcalc as cc
import pytest
from cavcalc.errors import CavCalcError


@pytest.mark.parametrize(
    "args",
    (
        ("FSR", {}),
        ("FWHM", {"gs": 0.9, "L": 1}),
        ("w0", {"L": 3.14, "R1": 0.9, "R2": 0.99}),
        ("gouy", {"Rc": 100}),
        ("div", {"gouy": 310, "T1": 0.014, "T2": 5e-6}),
        ("Aint", {"L": "5.6cm", "L1": 1e-5, "L2": "L1"}),
    ),
)
def test_incorrect_parameter_set(args):
    target, kwargs = args
    with pytest.raises(CavCalcError, match=f"Incorrect usage. To compute.*{target}.*require"):
        cc.calculate(target, **kwargs)
