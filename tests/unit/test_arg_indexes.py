import cavcalc as cc
import pytest


@pytest.mark.parametrize("target", ("w0", "gouy", None))
@pytest.mark.parametrize(
    "args",
    (
        {"L": "10km", "Rc": 5510},
        {"Rc": 30, "L": 35},
        {"L": 1, "g2": -0.95, "g1": -0.667},
        {"g1": 0.7, "g2": 0.4, "wl": 1550, "L": 103.3},
    ),
)
def test_arg_indexes(target, args):
    out = cc.calculate(target, **args)

    arg_names = tuple(args.keys())
    for name, arg in out.given.items():
        if name == "wl" and name not in arg_names:
            assert arg.index is None
        else:
            assert arg_names.index(name) == arg.index
