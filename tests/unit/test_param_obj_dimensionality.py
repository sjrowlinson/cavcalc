from cavcalc.parameters import ParameterCategory, TargetParameter
from cavcalc.parameters.tools import get_names
import pytest


@pytest.mark.parametrize(
    "name",
    get_names(
        ParameterCategory.BeamRadius,
        ParameterCategory.Curvature,
        ParameterCategory.Distance,
        ParameterCategory.Wave,
    ),
)
def test_param_has_length_units(name):
    p = TargetParameter(name)
    assert p.has_length_units
    assert not p.has_angular_units
    assert not p.has_frequency_units
    assert not p.is_unitless


@pytest.mark.parametrize("name", get_names(ParameterCategory.Angle, ParameterCategory.Phase))
def test_param_has_angular_units(name):
    p = TargetParameter(name)
    assert p.has_angular_units
    assert not p.has_length_units
    assert not p.has_frequency_units
    assert not p.is_unitless


@pytest.mark.parametrize("name", get_names(ParameterCategory.Frequency))
def test_param_has_freq_units(name):
    p = TargetParameter(name)
    assert p.has_frequency_units
    assert not p.has_angular_units
    assert not p.has_length_units
    assert not p.is_unitless


@pytest.mark.parametrize("name", get_names(ParameterCategory.Power, ParameterCategory.Stability))
def test_param_is_dimensionless(name):
    p = TargetParameter(name)
    assert p.is_unitless
    assert not p.has_angular_units
    assert not p.has_length_units
    assert not p.has_frequency_units
