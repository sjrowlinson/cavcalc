import cavcalc as cc
from cavcalc.output import SingleOutput, MultiOutput
import numbers
import numpy as np
import pytest


@pytest.mark.parametrize("compute", ("w", "w0", "gouy", "Rc"))
def test_scalar_single_target(compute):
    out = cc.calculate(compute, L=1, gs=-0.5)

    assert isinstance(out, SingleOutput)
    assert isinstance(out.result.value.m, numbers.Number)


def test_scalar_all_target_general():
    out = cc.calculate(L=4e-3, R1=0.9, R2=0.99)

    assert isinstance(out, MultiOutput)
    for result in out.results.values():
        assert isinstance(result.value.m, numbers.Number)


def test_scalar_all_target_symmetric():
    out = cc.calculate(L="10km", gs=0.8)

    assert isinstance(out, MultiOutput)
    for result in out.results.values():
        assert isinstance(result.value.m, numbers.Number)


@pytest.mark.parametrize("compute", ("w1", "w2", "z0"))
@pytest.mark.parametrize("num", (3, 11, 49))
def test_1D_array_single_target(compute, num):
    out = cc.calculate(compute, L="4km", g1=-1.07, g2=np.linspace(-0.8, -0.4, num))

    assert isinstance(out, SingleOutput)
    assert isinstance(out.result.value.m, np.ndarray)
    assert out.result.value.m.shape == (num,)


@pytest.mark.parametrize("num", (99, 3, 11))
def test_1D_array_all_target_general(num):
    out = cc.calculate(L=np.linspace(1, 10, num), R1=0.99, R2=0.999)

    assert isinstance(out, MultiOutput)

    not_length_dependent = ("finesse", "T1", "T2", "Aint", "Aext", "Atrn")
    for name, result in out.results.items():
        if name in not_length_dependent:
            assert isinstance(result.value.m, numbers.Number)
        else:
            assert isinstance(result.value.m, np.ndarray)
            assert result.value.m.shape == (num,)


@pytest.mark.parametrize("num", (3, 5, 49))
def test_1D_array_all_target_nonsymmetric(num):
    out = cc.calculate(L=33.3, g1=np.linspace(-1.1, -0.9, num), g2=-0.6)

    assert isinstance(out, MultiOutput)
    for name, result in out.results.items():
        if name == "FSR" or name == "Rc2":  # FSR, Rc2 not dependent on g1
            assert isinstance(result.value.m, numbers.Number)
        else:
            assert isinstance(result.value.m, np.ndarray)
            assert result.value.m.shape == (num,)


@pytest.mark.parametrize("L_pts", (3, 5, 10))
@pytest.mark.parametrize("R1_pts", (7, 13))
@pytest.mark.parametrize("R2_pts", (3, 9))
@pytest.mark.parametrize("meshes", ("L,R1,R2", "R1,R2,L", "R2,L,R1"))
def test_ND_array_meshed_target(L_pts, R1_pts, R2_pts, meshes):
    out = cc.calculate(
        "FWHM",  # this depends on L, R1, R2 so is a good case for testing mesh-grid construction
        L=np.linspace(0.1, 100, L_pts),
        R1=np.linspace(0.9, 0.99, R1_pts),
        R2=np.linspace(0.99, 0.999, R2_pts),
        meshes=meshes,
    )

    assert isinstance(out, SingleOutput)
    assert isinstance(out.result.value.m, np.ndarray)

    pts_map = {"L": L_pts, "R1": R1_pts, "R2": R2_pts}
    expect_shape = tuple(pts_map[pname] for pname in meshes.split(","))

    assert out.result.value.m.shape == expect_shape


# TODO (sjr) Add ND array all target test(s)


@pytest.mark.parametrize("compute", ("gs", "w0", "Rc", "gouy", "div"))
def test_dual_valued_single_target_symmetric(compute):
    out = cc.calculate(compute, L=10, g=0.9)

    assert isinstance(out, SingleOutput)
    assert isinstance(out.result.value.m, np.ndarray)
    assert out.result.value.m.shape == (2,)


@pytest.mark.parametrize("compute", ("g1", "g2", "Rc1", "Rc2"))
def test_dual_valued_single_target_nonsymmetric(compute):
    out = cc.calculate(compute, L="4km", w1=50, w2=60)

    assert isinstance(out, SingleOutput)
    assert isinstance(out.result.value.m, np.ndarray)
    assert out.result.value.m.shape == (2,)


# TODO (sjr) Add tests for shapes of dual valued targets with array args
