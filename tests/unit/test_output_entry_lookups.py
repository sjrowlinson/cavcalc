import cavcalc as cc
from cavcalc.errors import CavCalcError
from cavcalc.parameters import ParameterType as PT
import pytest


@pytest.mark.parametrize("entry", ("w1", "w0", "Rc1", "Rc2", "gouy"))
def test_existing_target_strings(entry):
    out = cc.calculate(L=1, g1=-0.4, g2=-0.9)

    assert out[entry]


@pytest.mark.parametrize("entry", (PT.BEAMSIZE_M2, PT.ROC_M1, PT.DIVERGENCE))
def test_existing_target_ptypes(entry):
    out = cc.calculate(L=1, g1=-0.4, g2=-0.9)

    assert out[entry]


@pytest.mark.parametrize("entry", ("FWHM", "T1", "T2", "Aint", "Atrn"))
def test_nonexisting_target_strings(entry):
    out = cc.calculate(L=1, g1=-0.4, g2=-0.9)

    with pytest.raises(CavCalcError, match=f"No target of name '{entry}'"):
        _ = out[entry]


@pytest.mark.parametrize("entry", (PT.FINESSE, PT.RES_ENHANCE_EXTERNAL, PT.TRANSMISSION_M2))
def test_nonexisting_target_ptypes(entry):
    out = cc.calculate(L=1, g1=-0.4, g2=-0.9)

    with pytest.raises(CavCalcError, match=f"No target of parameter type '{entry}'"):
        _ = out[entry]


@pytest.mark.parametrize("entry", (10, 3.14, True))
def test_invalid_type_lookup(entry):
    out = cc.calculate(L=1, g1=-0.4, g2=-0.9)

    with pytest.raises(TypeError, match="Expected key-type of str, ParameterType"):
        _ = out[entry]


def test_as_singles_with_no_args():
    out = cc.calculate(L=1, g1=-0.4, g2=-0.9)

    singles = out.as_singles()
    assert set(singles.keys()) == set(p.ptype for p in out.results.values())


@pytest.mark.parametrize(
    "entry", ("FWHM", "T1", PT.TRANSMISSION_M2, "Aint", PT.TRANSMISSION_INTENSITY_FRAC)
)
def test_as_singles_with_nonexisting_arg(entry):
    out = cc.calculate(L=1, g1=-0.4, g2=-0.9)

    with pytest.raises(CavCalcError, match=f"No target of type '{entry}'"):
        out.as_singles(entry)


@pytest.mark.parametrize("entry", (10, 3.14, True))
def test_as_singles_with_invalid_type_arg(entry):
    out = cc.calculate(L=1, g1=-0.4, g2=-0.9)

    with pytest.raises(CavCalcError, match="All arguments.*must be strings or ParameterType"):
        out.as_singles(entry)
